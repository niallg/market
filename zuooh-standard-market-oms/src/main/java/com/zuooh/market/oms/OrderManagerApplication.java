package com.zuooh.market.oms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderManagerApplication {

   public static void main(String[] list) {
      SpringApplication.run(OrderManagerApplication.class, list);
   }
}
