package com.zuooh.market.gdax;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=GdaxProperties.class)
public class GdaxConfiguration {

}
