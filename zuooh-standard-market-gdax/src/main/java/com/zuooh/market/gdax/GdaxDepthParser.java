package com.zuooh.market.gdax;

import static com.zuooh.market.gdax.GdaxField.ASKS;
import static com.zuooh.market.gdax.GdaxField.BIDS;
import static com.zuooh.market.gdax.GdaxField.BUY;
import static com.zuooh.market.gdax.GdaxField.CHANGES;
import static com.zuooh.market.gdax.GdaxField.PRODUCT_ID;
import static com.zuooh.market.gdax.GdaxField.SELL;
import static com.zuooh.market.gdax.GdaxField.SNAPSHOT_TYPE;
import static com.zuooh.market.gdax.GdaxField.TYPE;
import static com.zuooh.market.gdax.GdaxField.UPDATE_TYPE;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

//https://docs.gdax.com/?python#the-code-classprettyprinttickercode-channel
@Slf4j
public class GdaxDepthParser {
   
   private final ObjectMapper mapper;
   private final AtomicLong counter;
   
   public GdaxDepthParser(ObjectMapper mapper) {
      this.counter = new AtomicLong();
      this.mapper = mapper;
   }

   public Depth parseMessage(String source) {
      try {
         Map<String, Object> message = mapper.readValue(source, Map.class);
         Object type = message.get(TYPE);
         
         if(SNAPSHOT_TYPE.equals(type)) {
            return parseSnapshot(message);
         } 
         if(UPDATE_TYPE.equals(type)) {
            return parseLevel2Update(message);
         }
      } catch(Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
      return null;
   }
   /*
    * {
    "type": "snapshot",
    "product_id": "BTC-EUR",
    "bids": [["1", "2"]],
    "asks": [["2", "3"]]
}
    */
   private Depth parseSnapshot(Map<String, Object> message) {
      CurrencyPair pair = parseCurrencyPair(message);
      PriceSeries bids = parseSnapshotPriceList(pair, Side.BID, (List)message.get(BIDS));
      PriceSeries asks = parseSnapshotPriceList(pair, Side.ASK, (List)message.get(ASKS));
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, Market.GDAX, version, time);
   }
   
   /*
    * {
    "type": "l2update",
    "product_id": "BTC-EUR",
    "changes": [
        ["buy", "1", "3"],
        ["sell", "3", "1"],
        ["sell", "2", "2"],
        ["sell", "4", "0"]
    ]
}
    */
   private Depth parseLevel2Update(Map<String, Object> message) {
      CurrencyPair pair = parseCurrencyPair(message);
      List<List<?>> changes = (List)message.get(CHANGES);
      PriceSeries bids = parseUpdatePriceList(pair, Side.BID, BUY, changes);
      PriceSeries asks = parseUpdatePriceList(pair, Side.ASK, SELL, changes);                 
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.UPDATE, Market.GDAX, version, time);
   }
   
   private PriceSeries parseSnapshotPriceList(CurrencyPair pair, Side side, List<List<?>> lists) {
      List<Price> prices =lists.stream()
            .filter(Objects::nonNull)
            .filter(list -> !list.isEmpty())
            .map(list -> parsePrice(pair, side, list, 0))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }
   
   private PriceSeries parseUpdatePriceList(CurrencyPair pair, Side side, String filter, List<List<?>> list) {
      List<Price> prices = list.stream()
            .filter(Objects::nonNull)
            .filter(entry -> entry.get(0).equals(filter))
            .map(entry -> parsePrice(pair, side, entry, 1))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }
   
   private Price parsePrice(CurrencyPair pair, Side side, List<?> list, int start) {
      String price = list.get(start).toString();
      String quantity = list.get(start + 1).toString();
      
      return new Price(pair, 
            Market.GDAX, 
            side, 
            price,
            Double.parseDouble(price), 
            Double.parseDouble(quantity));
   }
   
   private CurrencyPair parseCurrencyPair(Map<String, Object> message) {
      String token = String.valueOf(message.get(PRODUCT_ID));
      String[] pair = token.toUpperCase().split("-");
      
      return new CurrencyPair(
            Currency.valueOf(pair[0]),
            Currency.valueOf(pair[1]));
   }
}
