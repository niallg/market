package com.zuooh.market.gdax;

public interface GdaxField {
   String SUBSCRIBE_TYPE = "subscribe"; // message type for subscribe request
   String SNAPSHOT_TYPE = "snapshot"; // message type for depth response
   String UPDATE_TYPE = "l2update"; // message type for depth update
   String PRODUCT_IDS = "product_ids";
   String PRODUCT_ID = "product_id";
   String CHANGES = "changes";
   String CHANNELS = "channels";
   String TICKER = "ticker";
   String LEVEL2 = "level2";
   String HEARTBEAT = "heartbeat";
   String NAME = "name";
   String TYPE = "type";
   String BIDS = "bids";
   String ASKS = "asks";
   String BUY = "buy";
   String SELL = "sell";
}
