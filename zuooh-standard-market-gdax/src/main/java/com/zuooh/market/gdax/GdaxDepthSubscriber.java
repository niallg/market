package com.zuooh.market.gdax;

import java.io.Closeable;
import java.net.URI;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;

import lombok.extern.slf4j.Slf4j;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthListener;
import com.zuooh.market.subscribe.DepthSubscriber;

@Slf4j
@Component
public class GdaxDepthSubscriber implements DepthSubscriber {

   private static final String DEPTH_URL = "wss://ws-feed.gdax.com";
   
   private final AtomicReference<WebSocketSubscriber> reference;
   private final DepthDistributor distributor;
   private final GdaxRequestBuilder builder;
   private final GdaxDepthParser parser;
   private final GdaxProperties properties;
   private final ObjectMapper mapper;
   private final URI address;
   
   public GdaxDepthSubscriber(GdaxProperties properties, DepthDistributor distributor) throws Exception {
      this.reference = new AtomicReference<WebSocketSubscriber>();
      this.mapper = new ObjectMapper();
      this.builder = new GdaxRequestBuilder(mapper);
      this.parser = new GdaxDepthParser(mapper);
      this.address = new URI(DEPTH_URL);
      this.properties = properties;
      this.distributor = distributor;
   }
   
   @Override
   public Set<CurrencyPair> subscribe() {
      if(properties.isEnabled()) {
         Set<CurrencyPair> pairs = properties.getPairs();
         pairs.stream().forEach(this::subscribe);
         return pairs;
      }
      return Collections.emptySet();
   }
   
   private void subscribe(CurrencyPair pair) {
      String message = builder.createSubscription(pair);
      
      try {   
         WebSocketSubscriber subscriber = connect(address);
    
         log.info("Successfully subscribed to: " + DEPTH_URL + " with [" + message + "]");
         subscriber.sendSubscription(message);
      } catch(Exception e) {
         throw new IllegalStateException("Could not subscribe for " + pair, e);
      }
   }
   
   private WebSocketSubscriber connect(URI address) {
      WebSocketSubscriber subscriber = reference.get();
      
      if(subscriber == null || subscriber.isClosed()) {
         try {
            subscriber = new WebSocketSubscriber(distributor, address);
            
            subscriber.connect();
            reference.set(subscriber);
         } catch(Exception e) {
            throw new IllegalStateException("Could not subscribe to " + address, e);
         }
      }
      return subscriber;
   }

   private class WebSocketSubscriber extends WebSocketClient implements Closeable{

      private final BlockingQueue<String> messages;
      private final DepthListener listener;
      
      public WebSocketSubscriber(DepthListener listener, URI address) {
         super(address);
         this.messages = new LinkedBlockingQueue<String>();
         this.listener = listener;
      }
      
      public void sendSubscription(String message) {
         messages.offer(message);
         flushMessages();
      }

      private void flushMessages() {
         if(isOpen()) {
            while(!messages.isEmpty()) {
               String message = messages.peek();
               
               try{
                  send(message);
                  messages.poll();
               }catch(Exception e){
                  log.info("Could not send subscription", e);
                  break;
               }
            }
         }
      }
      
      @Override
      public void onOpen(ServerHandshake handshakedata) {
         log.info("onOpen()");
         flushMessages();
      }

      @Override
      public void onMessage(String message) {
         Depth depth = parser.parseMessage(message);
         
//         if(depth != null) {
//            log.info("Got pair update " + depth.getPair());
//         }
         if(depth != null) {
            listener.onUpdate(Market.GDAX, depth);
         }
         flushMessages();
      }

      @Override
      public void onClose(int code, String reason, boolean remote) {
         log.info("onClose(" + code + ", " + reason + ")");
         reference.set(null);
      }

      @Override
      public void onError(Exception cause) { 
         log.info("onError()", cause);
         listener.onError(Market.GDAX, cause);
      }
   }
}
