package com.zuooh.market.gdax;

import static com.zuooh.market.gdax.GdaxField.CHANNELS;
import static com.zuooh.market.gdax.GdaxField.HEARTBEAT;
import static com.zuooh.market.gdax.GdaxField.LEVEL2;
import static com.zuooh.market.gdax.GdaxField.NAME;
import static com.zuooh.market.gdax.GdaxField.PRODUCT_IDS;
import static com.zuooh.market.gdax.GdaxField.SUBSCRIBE_TYPE;
import static com.zuooh.market.gdax.GdaxField.TICKER;
import static com.zuooh.market.gdax.GdaxField.TYPE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;

public class GdaxRequestBuilder {
   
   private final PrettyPrinter printer;
   private final ObjectMapper mapper;
   
   public GdaxRequestBuilder(ObjectMapper mapper) {
      this.printer = new DefaultPrettyPrinter();
      this.mapper = mapper;
   }
   
   public String createSubscription(CurrencyPair pair) {
      try {
         Map<String, Object> subscription = new LinkedHashMap<String, Object>();
         
         subscription.put(TYPE, SUBSCRIBE_TYPE);
         subscription.put(PRODUCT_IDS, createProductList(pair));
         subscription.put(CHANNELS, createChannels(pair));
         
         return mapper.writer(printer).writeValueAsString(subscription);
      } catch(Exception e) {
         throw new IllegalStateException("Could not create subscription", e);
      }
   }
   
   private List<String> createProductList(CurrencyPair pair) {
      return Arrays.asList(String.format("%s-%s", 
            pair.getBase(), 
            pair.getQuote()));
      
   }
   
   private List<Object> createChannels(CurrencyPair pair) {
      List<Object> channels = new ArrayList<Object>();
      Map<String, Object> ticker = new HashMap<String, Object>();

      ticker.put(NAME, TICKER);
      ticker.put(PRODUCT_IDS, createProductList(pair));
      channels.add(LEVEL2);
      channels.add(HEARTBEAT);
      channels.add(ticker);
      
      return channels; 
      
   }
}