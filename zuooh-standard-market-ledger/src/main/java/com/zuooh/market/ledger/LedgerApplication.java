package com.zuooh.market.ledger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LedgerApplication {

   public static void main(String[] list) {
      SpringApplication.run(LedgerApplication.class, list);
   }
}
