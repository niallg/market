package com.zuooh.market.bitstamp;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
public class BitstampDepthParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;

   public BitstampDepthParser(ObjectMapper mapper) {
      this.counter = new AtomicLong();
      this.mapper = mapper;
   }

   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         Map<String, Object> message = mapper.readValue(source, Map.class);
         
         if(message != null) {
            return parseOrderBook(pair, message);
         }
         log.info("Received message: " + source);
      } catch (Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
      return null;
   }
   
   private Depth parseOrderBook(CurrencyPair pair, Map<String, Object> orderBook) {
      PriceSeries bids = parseBidOrders(pair, orderBook);
      PriceSeries asks = parseAskOrders(pair, orderBook);
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, Market.BITSTAMP, version, time);
   }
   
   private PriceSeries parseBidOrders(CurrencyPair pair, Map<String, Object> orderBook) {
      List<?> orders = (List)orderBook.get("bids");
      List<Price> prices = orders.stream()
            .filter(Objects::nonNull)
            .filter(entry -> entry instanceof List)
            .map(entry -> new OrderBookResponse(pair, (List)entry, Side.BID))
            .map(entry -> parseActiveOrder(entry))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }
   
   private PriceSeries parseAskOrders(CurrencyPair pair, Map<String, Object> orderBook) {
      List<?> orders = (List)orderBook.get("asks");
      List<Price> prices = orders.stream()
            .filter(Objects::nonNull)
            .filter(entry -> entry instanceof List)
            .map(entry -> new OrderBookResponse(pair, (List)entry, Side.ASK))
            .map(entry -> parseActiveOrder(entry))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }
   
   private Price parseActiveOrder(OrderBookResponse response) {
      Side side = response.getSide();
      CurrencyPair pair = response.getPair();
      String order = response.getOrderId();
      double price = response.getPrice();
      double amount = response.getAmount();
      
      return new Price(pair, Market.BITSTAMP, side, order, Math.abs(price), Math.abs(amount));
   }
   
   private static List getListField(List list, int index) {
      Object value = list.get(index);
      
      if(List.class.isInstance(value)) {
         return (List)value;
      }
      return null;
   }
   
   private static Long getLongField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Long.parseLong(value);
      }
      return null;
   }

   private static Double getDoubleField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Double.parseDouble(value);
      }
      return null;
   }

   private static String getField(List list, int index) {
      Object value = list.get(index);

      if (value != null) {
         return String.valueOf(value);
      }
      return null;
   }
   
   @AllArgsConstructor
   private class OrderBookResponse {

      private final CurrencyPair pair;
      private final List<?> list;
      private final Side side;
      
      public Side getSide() {
         return side;
      }
      
      public CurrencyPair getPair(){
         return pair;
      }

      public String getOrderId() {
         return getField(list, BitstampField.OrderField.ORDER_ID);
      }
      
      public Double getAmount() {
         return getDoubleField(list, BitstampField.OrderField.AMOUNT);
      }

      public Double getPrice() {
         return getDoubleField(list, BitstampField.OrderField.PRICE);
      }
   }

}
