package com.zuooh.market.bitstamp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=BitstampProperties.class)
public class BitstampConfiguration {

}
