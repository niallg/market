package com.zuooh.market.bitstamp;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;

public class BitstampRequestBuilder {

   private final PrettyPrinter printer;
   private final ObjectMapper mapper;
   
   public BitstampRequestBuilder(ObjectMapper mapper) {
      this.printer = new DefaultPrettyPrinter();
      this.mapper = mapper;
   }
   
   /*
      "[
         {'event':'addChannel','channel':'ok_btcusd_ticker'},
         {'event':'addChannel','channel':'ok_btcusd_depth'},
         {'event':'addChannel','channel':'ok_btcusd_trades'}]" 
    */
   public String createSubscription(CurrencyPair pair) {
      try {
         Currency base = pair.getBase();
         Currency quote = pair.getQuote();
         String token = String.format("ok_%s%s_depth",base, quote).toLowerCase();
         SubscribeRequest request = new SubscribeRequest("addChannel", token, 0);
         return mapper.writer(printer).writeValueAsString(Arrays.asList(request));
      } catch(Exception e) {
         throw new IllegalStateException("Could not create subscription", e);
      }
   }
   
   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   private static class SubscribeRequest {
      
      private String event;
      private String channel;
      private int binary;
   }
}
