package com.zuooh.market.bitstamp;

public interface BitstampField {

   interface OrderField {
      int ORDER_ID = 0;
      int PRICE = 0;
      int AMOUNT = 1;
   }

}
