package com.zuooh.market.bitstamp;

import java.io.Closeable;
import java.net.URI;
import java.util.Collections;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthSubscriber;
import com.zuooh.market.subscribe.poller.PollerSubscriber;

@Slf4j
@Component
public class BitstampDepthSubscriber implements DepthSubscriber {

   private static final String DEPTH_URL = "https://www.bitstamp.net/api/v2/order_book/";
   
   private final DepthDistributor distributor;
   private final BitstampRequestBuilder builder;
   private final BitstampDepthParser parser;
   private final BitstampProperties properties;
   private final ObjectMapper mapper;
   private final URI address;
   
   public BitstampDepthSubscriber(BitstampProperties properties, DepthDistributor distributor) throws Exception {
      this.mapper = new ObjectMapper();
      this.builder = new BitstampRequestBuilder(mapper);
      this.parser = new BitstampDepthParser(mapper);
      this.address = new URI(DEPTH_URL);
      this.properties = properties;
      this.distributor = distributor;
   }
   
   @Override
   public Set<CurrencyPair> subscribe() {
      if(properties.isEnabled()) {
         Set<CurrencyPair> pairs = properties.getPairs();
         pairs.stream().forEach(this::subscribe);
         return pairs;
      }
      return Collections.emptySet();
   }
   
   private void subscribe(CurrencyPair pair) {
      String message = builder.createSubscription(pair);
      
      try {   
         connect(pair, address);
      } catch(Exception e) {
         throw new IllegalStateException("Could not subscribe for " + pair, e);
      }
   }
   
   private void connect(CurrencyPair pair, URI address) {
      String token = String.format("%s%s", pair.getBase(), pair.getQuote()).toLowerCase();
      String addr = address.toString()+token + "/";
      
      log.info("Successfully subscribed to: " + addr);
      new SubscriptionConnection(pair, addr).subscribe();
   }
   
   public class SubscriptionConnection extends PollerSubscriber {

      private final CurrencyPair pair;
      
      public SubscriptionConnection(CurrencyPair pair, String address) {
         super(address);
         this.pair = pair;
      }

      @Override
      public void onResponse(String message) {
         Depth depth = parser.parseDepth(pair, message);
         if(depth != null) {
            distributor.onUpdate(Market.BITSTAMP, depth);
         }
      }
      
   }
}
