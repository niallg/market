package com.zuooh.market.binance;

import static com.zuooh.market.Market.BINANCE;

import java.io.Closeable;
import java.net.URI;
import java.util.Collections;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.stereotype.Component;

import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthListener;
import com.zuooh.market.subscribe.DepthSubscriber;

@Slf4j
//@Component
public class BinanceDepthSubscriber implements DepthSubscriber {

   private static final String DEPTH_URL = "wss://stream.binance.com:9443/ws/%s%s@depth";
   
   private final DepthDistributor distributor;
   private final BinanceDepthParser parser;
   private final BinanceProperties properties;
   
   public BinanceDepthSubscriber(BinanceProperties properties, DepthDistributor distributor) {
      this.parser = new BinanceDepthParser();
      this.properties = properties;
      this.distributor = distributor;
   }
   
   
   @Override
   public Set<CurrencyPair> subscribe() {
      if(properties.isEnabled()) {
         Set<CurrencyPair> pairs = properties.getPairs();
         pairs.stream().forEach(this::subscribe);
         return pairs;
      }
      return Collections.emptySet();
   }
   
	private void subscribe(CurrencyPair pair) {
	   Currency quote = pair.getQuote();
	   Currency base = pair.getBase();
	   String target = String.format(DEPTH_URL, base, quote).toLowerCase();
	   
	   try {
	      URI address = new URI(target);
         WebSocketSubscriber subscriber = new WebSocketSubscriber(distributor, address);
      
         log.info("Successfully subscribed to: " + target);
         subscriber.connect();
	   } catch(Exception e) {
	      throw new IllegalStateException("Could not subscribe for " + pair, e);
	   }
   }

	private class WebSocketSubscriber extends WebSocketClient implements Closeable{

	   private final DepthListener listener;

	   public WebSocketSubscriber(DepthListener listener, URI address) {
	      super(address);
	      this.listener = listener;
	   }
	   
		@Override
		public void onOpen(ServerHandshake handshakedata) {
		   log.info("onOpen()");
		}

		@Override
		public void onMessage(String message) {
		   Depth depth = parser.parseDepth(message);
		   
		   if(depth != null){
		      listener.onUpdate(BINANCE, depth);
		   }
		}

		@Override
		public void onClose(int code, String reason, boolean remote) {
         log.info("onClose(" + code + ", " + reason + ")");
		}

		@Override
		public void onError(Exception cause) {  
		   listener.onError(BINANCE, cause);
		}
	}
}
