package com.zuooh.market.binance;

import javax.annotation.PostConstruct;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import com.zuooh.common.html.TableDrawer;
import com.zuooh.market.Currency;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.account.Order;
import com.zuooh.market.account.OrderBroker;
import com.zuooh.market.account.OrderRepository;
import com.zuooh.market.account.OrderStatus;

@Slf4j
@Component
@AllArgsConstructor
@ManagedResource(description="Binance order broker")
public class BinanceOrderBroker implements OrderBroker {

   private OrderRepository repository;
   
   @ManagedOperation(description="Show current orders")
   public String showOrders() {
      TableDrawer table = new TableDrawer("id", "pair", "quantity", "price", "side", "status");
      
      try {
         Iterable<Order> orders = repository.findAll();
         
         for(Order order : orders) {
            Long id = order.getId();
            Currency base = order.getBase();
            Currency quote = order.getQuote();
            double quantity = order.getQuantity();
            double price = order.getPrice();
            OrderStatus status = order.getStatus();
            Side side = order.getSide();
            
            table.newRow(id, base + "/" + quote, quantity, price, side, status);
         }
      } catch(Exception e){
         log.info("Could not find orders", e);
      }
      return table.toString();
   }
   
   @PostConstruct
   public void loadAll() {
      Order order = Order.builder()
            .base(Currency.ETH)
            .quote(Currency.USD)
            .quantity(100)
            .price(100.0)
            .market(Market.BINANCE)
            .side(Side.BID)
            .status(OrderStatus.NEW)
            .time(System.currentTimeMillis())
            .build();
      
      repository.save(order);
      log.info("repository: "+repository);
   }
   
   @Override
   public void execute(Order order) {
      // TODO Auto-generated method stub
      
   }

}
