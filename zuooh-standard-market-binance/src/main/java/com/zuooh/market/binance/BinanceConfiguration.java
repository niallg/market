package com.zuooh.market.binance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;

@Configuration
@ComponentScan(basePackageClasses=BinanceProperties.class)
public class BinanceConfiguration {
   
   @Value("${binance.apiKey:#{null}}")
   private String apiKey;
   
   
   @Value("${binance.secret:#{null}}")
   private String secret;
   
   @Bean
   public BinanceExchange getExchange() {
      if(apiKey != null && secret != null) {
         BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(apiKey, secret);
         BinanceApiRestClient client = factory.newRestClient();
         
         return new BinanceExchange(client);
      }
      return null;
   }
}
