package com.zuooh.market.binance;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

import com.binance.api.client.BinanceApiRestClient;
import com.zuooh.market.Currency;
import com.zuooh.market.Market;
import com.zuooh.market.account.Balance;
import com.zuooh.market.account.Exchange;

@Slf4j
public class BinanceExchange implements Exchange {
   
   private final BinanceApiRestClient client;
   
   public BinanceExchange(BinanceApiRestClient client) {
      this.client = client;
   }

   @Override
   public List<Balance> getBalances() {
      if(client != null) {
         return client.getAccount().getBalances()
            .stream()
            .filter(Objects::nonNull)
            .filter(balance -> {
               String asset = balance.getAsset();
               
               try {
                  Currency.valueOf(asset);
                  return true;
               } catch(Exception e) {
                  //log.info("We have a balance in " + asset + " yet we cannot process", e);
                  return false;
               }
            })
            .map(balance -> {   
               String asset = balance.getAsset();
               Currency currency = Currency.valueOf(asset);
               String free = balance.getFree();
               Double quantity = Double.parseDouble(free);
               
               return new Balance(Market.BINANCE, currency, quantity);
            })
            .filter(balance -> balance.getQuantity() > 0)
            .collect(Collectors.toList());
      }
      return Collections.emptyList();
   }

}
