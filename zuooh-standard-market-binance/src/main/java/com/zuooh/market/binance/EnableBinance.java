package com.zuooh.market.binance;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.zuooh.market.MarketConfiguration;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({MarketConfiguration.class, BinanceConfiguration.class})
public @interface EnableBinance {
}
