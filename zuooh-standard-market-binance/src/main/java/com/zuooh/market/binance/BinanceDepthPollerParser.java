package com.zuooh.market.binance;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

public class BinanceDepthPollerParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;
   
   public BinanceDepthPollerParser() {
      this.mapper = new ObjectMapper();
      this.counter = new AtomicLong();
   }
   
   /*
    * {"e":"depthUpdate","E":1513527409802,"s":"ETHBTC","U":49422118,"u":49422123,"b":[["0.03657200","11.94200000",[]],["0.03657100","0.00000000",[]]],"a":[["0.03660900","0.12000000",[]]]}
    */
   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         BinanceMarketDepth depth = mapper.readValue(source, BinanceMarketDepth.class);
         PriceSeries bids = parsePriceList(pair, Side.BID, depth.bids);
         PriceSeries asks = parsePriceList(pair, Side.ASK, depth.asks);
         long version = counter.getAndIncrement();
         long time = System.currentTimeMillis();
         
         return new Depth(pair, bids, asks, DepthType.UPDATE, Market.BINANCE, version, time);
      } catch(Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
   }
   
   private CurrencyPair parsePair(String symbol) {
      String base = symbol.substring(0, 3).toUpperCase();
      String quote = symbol.substring(3).toUpperCase();
      
      return new CurrencyPair(
            Currency.valueOf(base),
            Currency.valueOf(quote));
   }

   private PriceSeries parsePriceList(CurrencyPair pair, Side side, List<List<?>> lists) {
      List<Price> prices =lists.stream()
            .filter(Objects::nonNull)
            .filter(list -> !list.isEmpty())
            .map(list -> parsePrice(pair, side, list))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }
   
   private Price parsePrice(CurrencyPair pair, Side side, List<?> list) {
      String price = list.get(0).toString();
      String quantity = list.get(1).toString();
      
      return new Price(pair, 
            Market.BINANCE, 
            side, 
            price,
            Double.parseDouble(price),
            Double.parseDouble(quantity));
   }

   /*
   {  
   "lastUpdateId":24319139,
   "bids":[  
      [  
         "0.01944300",
    */
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class BinanceMarketDepth {
      
      public List<List<?>> bids; // bids
      public List<List<?>> asks; // asks
      public String lastUpdateId; // id
   }
}
