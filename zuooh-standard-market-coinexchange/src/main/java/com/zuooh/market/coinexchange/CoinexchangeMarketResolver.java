package com.zuooh.market.coinexchange;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;

@Slf4j
public class CoinexchangeMarketResolver {

   private final String MARKET_URL = "https://www.coinexchange.io/api/v1/getmarkets";
   
   private final Map<CurrencyPair, String> marketIds;
   private final ObjectMapper mapper;
   
   public CoinexchangeMarketResolver() {
      this.marketIds = new ConcurrentHashMap<CurrencyPair, String>();
      this.mapper = new ObjectMapper();
   }
   
   public String getMarketId(CurrencyPair pair) {
      if(marketIds.isEmpty()) {
         getMarkets().stream()
            .forEach(definition -> {
               CurrencyPair key = definition.getPair();
               String value = definition.getId();
               
               if(key != null) {
                  marketIds.put(key, value);
               }
            });
      }
      return marketIds.get(pair);
   }
   
   private List<MarketDefinition> getMarkets() {
      try {
         InputStream stream = new URL(MARKET_URL).openStream();
         return mapper.readValue(stream, Markets.class).getResult();
      }catch(Exception e){
         log.error("Could not get markets", e);
         return null;
      }
   }
   /*
    * {  
   "success":"1",
   "request":"\/api\/v1\/getmarkets",
   "message":"",
   "result":[  
      {  
         "MarketID":"18",
         "MarketAssetName":"Litecoin",
         "MarketAssetCode":"LTC",
         "MarketAssetID":"2",
         "MarketAssetType":"currency",
         "BaseCurrency":"Bitcoin",
         "BaseCurrencyCode":"BTC",
         "BaseCurrencyID":"1",
         "Active":true
      },
    */
   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class MarketDefinition {
      
      private String MarketID;
      private String MarketAssetName;
      private String MarketAssetCode;
      private String MarketAssetID;
      private String MarketAssetType;
      private String BaseCurrency;
      private String BaseCurrencyCode;
      private String BaseCurrencyID;
      private String Active;
      
      public String getId() {
         return MarketID;
      }
      
      public CurrencyPair getPair() {
         try {
            return new CurrencyPair(
                  Currency.valueOf(MarketAssetCode.toUpperCase()),
                  Currency.valueOf(BaseCurrencyCode.toUpperCase())
                  );
         } catch(Exception e) {
            return null;
         }
      }
   }
   
   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class Markets {
   
      private List<MarketDefinition> result;
   }
}
