package com.zuooh.market.coinexchange;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
public class CoinexchangeDepthPollerParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;

   public CoinexchangeDepthPollerParser() {
      this.mapper = new ObjectMapper();
      this.counter = new AtomicLong();
   }
   

   /*
    {  
   "success":true,
   "code":"OK",
   "msg":"Operation succeeded.",
   "timestamp":1515874531333,
   "data":{  
      "SELL":[  
         [  
            0.09889287,
            0.727574,
            0.07195188
         ],
    */
   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         OrderBook book = mapper.readValue(source, OrderBook.class);
         
         if(log.isTraceEnabled()) {
            log.trace("Received message: " + source);
         }
         return parseOrderBook(pair, book);
      } catch (Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
   }
   
   private Depth parseOrderBook(CurrencyPair pair, OrderBook book) {
      PriceSeries bids = parsePriceList(pair, Side.BID, book.getResult().getBuy());
      PriceSeries asks = parsePriceList(pair, Side.ASK, book.getResult().getSell());
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, Market.COINEXCHANGE, version, time);
   }
   
   private PriceSeries parsePriceList(CurrencyPair pair, Side side, List<OrderData> prices) {
     List<Price> matches = prices.stream()
           .filter(Objects::nonNull)
           .map(data -> parseActiveOrder(pair, side, data))
           .collect(Collectors.toList());
     
     return new PriceSeries(matches);
   }
   
   
   private Price parseActiveOrder(CurrencyPair pair, Side side, OrderData data) {
      String order = data.getId();
      double price = data.getPrice();
      double amount = data.getQuantity();
      
      return new Price(pair, Market.COINEXCHANGE, side, order, Math.abs(price), Math.abs(amount));
   }

/*
 * {  
   "success":"1",
   "request":"\/api\/v1\/public\/getorderbook",
   "message":"",
   "result":{  
      "SellOrders":[  
         {  
            "Type":"sell",
            "Price":"0.01752830",
            "OrderTime":"2018-01-13 21:14:27",
            "Quantity":"0.46903000"
         },
 */
   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderBook {
      
      private OrderBookData result;
   }
   
   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderBookData {
      
      private List<OrderData> SellOrders;
      private List<OrderData> BuyOrders;
      
      public List<OrderData> getSell(){
         return SellOrders;
      }
      
      public List<OrderData> getBuy(){
         return BuyOrders;
      }
   }

   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderData {
      
      private String Type;
      private Double Price;
      private String OrderTime;
      private Double Quantity;
      
      public String getId(){
         return String.valueOf(Price);
      }
      
      public double getPrice(){
         return Price;
      }
      
      public double getQuantity(){
         return Quantity;
      }
   }

}
