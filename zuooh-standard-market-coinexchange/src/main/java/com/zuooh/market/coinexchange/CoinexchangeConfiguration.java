package com.zuooh.market.coinexchange;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=CoinexchangeProperties.class)
public class CoinexchangeConfiguration {
}
