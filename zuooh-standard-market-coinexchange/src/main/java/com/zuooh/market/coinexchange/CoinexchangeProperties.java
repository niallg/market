package com.zuooh.market.coinexchange;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zuooh.market.CurrencyPair;

@Component
public class CoinexchangeProperties {

   @Value("${coinexchange.pairs}")
   private String[] pairs;
   
   @Value("${coinexchange.charges}")
   private double charges;
   
   @Value("${coinexchange.enabled:true}")
   private boolean enabled;
  
   public boolean isEnabled() {
      return enabled;
   }
   
   public double getCharges() {
      return charges;
   }
   
   public Set<CurrencyPair> getPairs() {
      return Arrays.asList(pairs)
            .stream()
            .filter(Objects::nonNull)
            .filter(pair -> {
               try {
                  CurrencyPair.parsePair(pair);
                  return true;
               }catch(Exception e){
                  return false;
               }
            })
            .map(pair -> CurrencyPair.parsePair(pair))
            .collect(Collectors.toSet());
   }
  
}
