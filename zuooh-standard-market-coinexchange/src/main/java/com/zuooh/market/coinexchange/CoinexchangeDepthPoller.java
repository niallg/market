package com.zuooh.market.coinexchange;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthSubscriber;
import com.zuooh.market.subscribe.poller.PollerSubscriber;

@Slf4j
@Component
public class CoinexchangeDepthPoller implements DepthSubscriber {
   
   private static final String DEPTH_URL = "https://www.coinexchange.io/api/v1/getorderbook";

   private final CoinexchangeMarketResolver resolver;
   private final CoinexchangeDepthPollerParser parser;
   private final CoinexchangeProperties properties;
   private final DepthDistributor distributor;
   
   public CoinexchangeDepthPoller(CoinexchangeProperties properties, DepthDistributor distributor) {
      this.resolver = new CoinexchangeMarketResolver();
      this.parser = new CoinexchangeDepthPollerParser();
      this.properties = properties;
      this.distributor = distributor;
   }
   
   
   @Override
   public Set<CurrencyPair> subscribe() {
      if(properties.isEnabled()) {
         Set<CurrencyPair> pairs = properties.getPairs();
         pairs.stream().forEach(this::subscribe);
         return pairs;
      }
      return Collections.emptySet();
   }
   
   private void subscribe(CurrencyPair pair) {
      try {   
         String marketId = resolver.getMarketId(pair);
         
         if(marketId != null) {
            connect(pair, new URI(DEPTH_URL), marketId);
         }
      } catch(Exception e) {
         throw new IllegalStateException("Could not subscribe for " + pair, e);
      }
   }
   
   private void connect(CurrencyPair pair, URI address, String marketId) {
      String addr = address.toString()+"?market_id="+marketId;
      
      log.info("Successfully subscribed to: " + addr);
      new SubscriptionConnection(pair, addr).subscribe();
   }
   
   public class SubscriptionConnection extends PollerSubscriber {

      private final CurrencyPair pair;
      
      public SubscriptionConnection(CurrencyPair pair, String address) {
         super(address);
         this.pair = pair;
      }

      @Override
      public void onResponse(String message) {
         Depth depth = parser.parseDepth(pair, message);
         if(depth != null) {
            distributor.onUpdate(Market.COINEXCHANGE, depth);
         }
      }
      
   }
}
