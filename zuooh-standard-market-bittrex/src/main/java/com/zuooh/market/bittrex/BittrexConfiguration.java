package com.zuooh.market.bittrex;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=BittrexProperties.class)
public class BittrexConfiguration {
}
