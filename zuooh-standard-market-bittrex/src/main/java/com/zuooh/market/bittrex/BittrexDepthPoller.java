package com.zuooh.market.bittrex;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthSubscriber;
import com.zuooh.market.subscribe.poller.PollerSubscriber;

@Slf4j
@Component
public class BittrexDepthPoller implements DepthSubscriber {
   
   private static final String DEPTH_URL = "https://bittrex.com/api/v1.1/public/getorderbook";
   
   private final DepthDistributor distributor;
   private final BittrexDepthPollerParser parser;
   private final BittrexProperties properties;
   
   public BittrexDepthPoller(BittrexProperties properties, DepthDistributor distributor) {
      this.parser = new BittrexDepthPollerParser();
      this.properties = properties;
      this.distributor = distributor;
   }
   
   
   @Override
   public Set<CurrencyPair> subscribe() {
      if(properties.isEnabled()) {
         Set<CurrencyPair> pairs = properties.getPairs();
         pairs.stream().forEach(this::subscribe);
         return pairs;
      }
      return Collections.emptySet();
   }
   
   private void subscribe(CurrencyPair pair) {
      try {   
         connect(pair, new URI(DEPTH_URL));
      } catch(Exception e) {
         throw new IllegalStateException("Could not subscribe for " + pair, e);
      }
   }
   
   private void connect(CurrencyPair pair, URI address) {
      String token = String.format("%s-%s", pair.getQuote(), pair.getBase()).toUpperCase();
      String addr = address.toString()+"?market="+token+"&type=both";
      
      log.info("Successfully subscribed to: " + addr);
      new SubscriptionConnection(pair, addr).subscribe();
   }
   
   public class SubscriptionConnection extends PollerSubscriber {

      private final CurrencyPair pair;
      
      public SubscriptionConnection(CurrencyPair pair, String address) {
         super(address);
         this.pair = pair;
      }

      @Override
      public void onResponse(String message) {
         Depth depth = parser.parseDepth(pair, message);
         if(depth != null) {
            distributor.onUpdate(Market.BITTREX, depth);
         }
      }
      
   }
}
