package com.zuooh.market.bittrex;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

public class BittrexDepthPollerParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;
   
   public BittrexDepthPollerParser() {
      this.mapper = new ObjectMapper();
      this.counter = new AtomicLong();
   }
   
   /*
    * {"e":"depthUpdate","E":1513527409802,"s":"ETHBTC","U":49422118,"u":49422123,"b":[["0.03657200","11.94200000",[]],["0.03657100","0.00000000",[]]],"a":[["0.03660900","0.12000000",[]]]}
    */
   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         OrderBook depth = mapper.readValue(source, OrderBook.class);
         PriceSeries bids = parsePriceList(pair, Side.BID, depth.result.buy);
         PriceSeries asks = parsePriceList(pair, Side.ASK, depth.result.sell);
         long version = counter.getAndIncrement();
         long time = System.currentTimeMillis();
         
         return new Depth(pair, bids, asks, DepthType.UPDATE, Market.BITTREX, version, time);
      } catch(Exception e) {
         throw new IllegalStateException("Could not parse depth " +pair+ " message: " + source, e);
      }
   }

   private PriceSeries parsePriceList(CurrencyPair pair, Side side, List<Order> orders) {
      List<Price> prices = orders.stream()
            .filter(Objects::nonNull)
            .map(order -> parsePrice(pair, side, order))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }
   
   private Price parsePrice(CurrencyPair pair, Side side, Order order) {
      double price = order.getRate();
      double quantity = order.getQuantity();
      
      return new Price(pair, 
            Market.BITTREX, 
            side, 
            String.valueOf(price),
            price,
            quantity);
   }

   /*
   {  
   "lastUpdateId":24319139,
   "bids":[  
      [  
         "0.01944300",
    */
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderBook {
      
      public OrderResult result;
      public String message; 
      public boolean success;
   }
   
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderResult {
      
      public List<Order> buy; // bids
      public List<Order> sell; // asks
   }
   
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class Order {
      
      public double Rate;
      public double Quantity;
   }
}
