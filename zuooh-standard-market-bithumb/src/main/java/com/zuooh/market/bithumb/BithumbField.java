package com.zuooh.market.bithumb;

public interface BithumbField {

   interface OrderField {
      int ORDER_ID = 0;
      int PRICE = 0;
      int AMOUNT = 1;
   }

}
