package com.zuooh.market.bithumb;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
public class BithumbDepthParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;

   public BithumbDepthParser(ObjectMapper mapper) {
      this.counter = new AtomicLong();
      this.mapper = mapper;
   }

   /*
    * {
    "status"    : "0000",
    "data"      : {
        "timestamp"         : 1417142049868,
        "order_currency"    : "BTC",
        "payment_currency"  : "KRW",
        "bids": [
            {
                "quantity"  : "6.1189306",
                "price"     : "504000"
            },
            {
                "quantity"  : "10.35117828",
                "price"     : "503000"
            }
        ],
        "asks": [
            {
                "quantity"  : "2.67575",
                "price"     : "506000"
            },
            {
                "quantity"  : "3.54343",
                "price"     : "507000"
            }
        ]
    }
}
    */

   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         OrderBook orderBook = mapper.readValue(source, OrderBook.class);
         log.info("Received message: " + source);
         return parseOrderBook(pair, orderBook);
      } catch (Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
   }
   
   private Depth parseOrderBook(CurrencyPair pair, OrderBook orderBook) {
      PriceSeries bids = parseOrders(pair, orderBook, Side.BID);
      PriceSeries asks = parseOrders(pair, orderBook, Side.ASK);
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, Market.BITHUMB, version, time);
   }
   
   private PriceSeries parseOrders(CurrencyPair pair, OrderBook orderBook, Side side) {
      List<Price> prices = orderBook.getData()
            .getOrders(side)
            .stream()
            .filter(Objects::nonNull)
            .map(entry -> parseOrder(pair, entry, side))
            .collect(Collectors.toList());
      
      return new PriceSeries(prices);
   }

   private Price parseOrder(CurrencyPair pair, Order order, Side side) {
      double price = order.getPrice();
      double amount = order.getQuantity();
      String key = String.valueOf(price);
      
      return new Price(pair, Market.BITHUMB, side, key, price, amount);
   }
   
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderBook {
      private String status;
      private OrderData data;
   }
   
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class OrderData {
      private Currency order_currency;
      private Currency payment_currency;
      private List<Order> bids;
      private List<Order> asks;
      private long timestamp;
      
      public List<Order> getOrders(Side side) {
         if(side == Side.BID) {
            return bids;
         }
         return asks;
      }
   }
   
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class Order {
      private double quantity;
      private double price;
   }
}
