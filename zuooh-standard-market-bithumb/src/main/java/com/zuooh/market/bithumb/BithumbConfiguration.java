package com.zuooh.market.bithumb;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=BithumbProperties.class)
public class BithumbConfiguration {

}
