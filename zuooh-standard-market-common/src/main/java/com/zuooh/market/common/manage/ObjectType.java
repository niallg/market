package com.zuooh.market.common.manage;

public enum ObjectType {
   OBJECT, 
   MAP, 
   COLLECTION, 
   ARRAY, 
   PRIMITIVE, 
   NULL;
}
