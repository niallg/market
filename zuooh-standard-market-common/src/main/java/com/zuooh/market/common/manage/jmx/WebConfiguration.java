package com.zuooh.market.common.manage.jmx;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WebConfiguration {

   private final String color;
   private final String password;
   private final String login;
   private final int port;

   public WebConfiguration(
         @Value("${jmx.color}") String color, 
         @Value("${jmx.login}") String login, 
         @Value("${jmx.password}") String password, 
         @Value("${jmx.port}") int port) 
   {
      this.password = password;
      this.color = color;
      this.login = login;
      this.port = port;
   }

   public String getColor() {
      return color;
   }

   public String getPassword() {
      return password;
   }

   public String getLogin() {
      return login;
   }

   public int getPort() {
      return port;
   }
}
