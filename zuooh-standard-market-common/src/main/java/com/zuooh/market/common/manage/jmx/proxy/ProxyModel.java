package com.zuooh.market.common.manage.jmx.proxy;

import javax.management.MBeanException;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;
import javax.management.modelmbean.ModelMBeanInfo;

import lombok.extern.slf4j.Slf4j;

import org.springframework.jmx.export.SpringModelMBean;

@Slf4j
public class ProxyModel extends SpringModelMBean {

   public ProxyModel() throws MBeanException, RuntimeOperationsException {
      super();
   }
   
   public ProxyModel(ModelMBeanInfo info) throws MBeanException, RuntimeOperationsException {
      super(info);
   }
   
   public Object invoke(String name, Object[] arguments, String[] signature) throws MBeanException, ReflectionException {
      try {
         return super.invoke(name, arguments, signature);
      } catch(MBeanException cause) {
         log.info("Error invoking " + name, cause);
         throw cause;
      } catch(ReflectionException cause) {
         log.info("Error invoking " + name, cause);
         throw cause;
      }      
   }
}
