package com.zuooh.market.common.manage.jmx.proxy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jmx.support.ConnectorServerFactoryBean;

@Configuration
public class ProxyModelConfiguration {

   @Value("${jmx.port}")
   private int port;

   @Bean
   public ConnectorServerFactoryBean connectionServer() {
      ConnectorServerFactoryBean factory = new ConnectorServerFactoryBean();
      factory.setServiceUrl("service:jmx:p2p://localhost:1" + port);
      return factory;
   }
}
