package com.zuooh.market.common.manage.properties;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource(description="Custom system properties for the application")
public class SystemProperties {

   private final Map<String, String> systemProperties;

   public SystemProperties() {
      this.systemProperties = new LinkedHashMap<String, String>();
   }

   @ManagedOperation(description="Set a custom property")
   @ManagedOperationParameters({ 
      @ManagedOperationParameter(name="propertyName", description="Property name"), 
      @ManagedOperationParameter(name="propertyValue", description="Property value") 
   })
   public void setProperty(String propertyName, String propertyValue) {
      systemProperties.put(propertyName, propertyValue);
   }

   @ManagedOperation(description="Get a custom property")
   @ManagedOperationParameters({ 
      @ManagedOperationParameter(name="propertyName", description="Property name") 
   })
   public String getProperty(String propertyName) {
      return systemProperties.get(propertyName);
   }

   @ManagedOperation(description="Apply to system properties")
   public void applyProperties() {
      System.getProperties().putAll(systemProperties);
   }
}
