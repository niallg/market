package com.zuooh.market.common.manage;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.zuooh.market.common.manage.jmx.proxy.ProxyModelConfiguration;

@Configuration
@Import(ProxyModelConfiguration.class)
@ComponentScan(basePackageClasses=ObjectInfo.class)
public class ManageConfiguration {

}
