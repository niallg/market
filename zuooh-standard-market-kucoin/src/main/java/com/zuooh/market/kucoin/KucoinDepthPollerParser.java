package com.zuooh.market.kucoin;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
public class KucoinDepthPollerParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;

   public KucoinDepthPollerParser() {
      this.mapper = new ObjectMapper();
      this.counter = new AtomicLong();
   }
   

   /*
    {  
   "success":true,
   "code":"OK",
   "msg":"Operation succeeded.",
   "timestamp":1515874531333,
   "data":{  
      "SELL":[  
         [  
            0.09889287,
            0.727574,
            0.07195188
         ],
    */
   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         Object message = mapper.readValue(source, Object.class);
         
         if(Map.class.isInstance(message)) {
            Map map = (Map)message;
            Map data = (Map)map.get(KucoinField.ORDERS);
            
            if(data != null) {
               return parseOrderBook(pair, data);
            }
         }
         if(log.isTraceEnabled()) {
            log.trace("Received message: " + source);
         }
      } catch (Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
      return null;
   }
   
   private Depth parseOrderBook(CurrencyPair pair, Map<?, ?> orders) {
      PriceSeries bids = parsePriceList(pair, Side.BID, (List)orders.get(KucoinField.BUY_FIELD));
      PriceSeries asks = parsePriceList(pair, Side.ASK, (List)orders.get(KucoinField.SELL_FIELD));
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, Market.KUCOIN, version, time);
   }
   
   private PriceSeries parsePriceList(CurrencyPair pair, Side side, List<?> prices) {
     List<Price> matches = parseActiveOrders(pair, side, prices).stream()
           .filter(price -> price.getPair().equals(pair))
           .filter(price -> price.getSide().equals(side))
           .collect(Collectors.toList());
     
     return new PriceSeries(matches);
   }
   
   private List<Price> parseActiveOrders(CurrencyPair pair, Side side, List<?> orders) {
      if(orders != null) {
         return orders.stream()
               .filter(Objects::nonNull)
               .filter(entry -> entry instanceof List)
               .map(entry -> new OrderBookResponse(pair, (List)entry))
               .map(entry -> parseActiveOrder(entry))
               .collect(Collectors.toList());
      }
      return Collections.emptyList();
   }
   
   private Price parseActiveOrder(OrderBookResponse response) {
      Side side = parseSide(response);
      CurrencyPair pair = response.getPair();
      String order = response.getOrderId();
      double price = response.getPrice();
      double amount = response.getAmount();
      
      return new Price(pair, Market.KUCOIN, side, order, Math.abs(price), Math.abs(amount));
   }

   private Side parseSide(OrderBookResponse response) {
      double quantity = response.getAmount();
      return quantity >= 0 ? Side.BID : Side.ASK; 
   }
   
   private static List getListField(List list, int index) {
      Object value = list.get(index);
      
      if(List.class.isInstance(value)) {
         return (List)value;
      }
      return null;
   }
   
   private static Long getLongField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Long.parseLong(value);
      }
      return null;
   }

   private static Double getDoubleField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Double.parseDouble(value);
      }
      return null;
   }

   private static String getField(List list, int index) {
      Object value = list.get(index);

      if (value != null) {
         return String.valueOf(value);
      }
      return null;
   }
   
   @AllArgsConstructor
   private class OrderBookResponse {

      private final CurrencyPair pair;
      private final List<?> list;
      
      public CurrencyPair getPair(){
         return pair;
      }

      public String getOrderId() {
         return getField(list, KucoinField.PRICE);
      }
      
      public Double getPrice() {
         return getDoubleField(list, KucoinField.PRICE);
      }
      
      public Double getAmount() {
         return getDoubleField(list, KucoinField.AMOUNT);
      }

      public Double getVolume() {
         return getDoubleField(list, KucoinField.VOLUME);
      }
   }

}
