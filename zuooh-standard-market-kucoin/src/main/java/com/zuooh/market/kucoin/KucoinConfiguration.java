package com.zuooh.market.kucoin;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=KucoinProperties.class)
public class KucoinConfiguration {
}
