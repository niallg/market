package com.zuooh.market.kucoin;

public interface KucoinField {
   String TIMESTAMP = "timestamp";
   String ORDERS = "data";
   String BUY_FIELD = "BUY";
   String SELL_FIELD = "SELL";
   int PRICE = 0;
   int AMOUNT = 1;
   int VOLUME = 2;
}
