package com.zuooh.market.okex.test;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.okex.OkexDepthParser;
import com.zuooh.market.okex.OkexRequestBuilder;
import com.zuooh.market.okex.WebSocketBase;
import com.zuooh.market.okex.WebSocketService;
import com.zuooh.market.subscribe.DepthDistributor;

public class BuissnesWebSocketServiceImpl implements WebSocketService{
	private Logger log = Logger.getLogger(WebSocketBase.class);
	
	private final DepthDistributor distributor;
	private final OkexDepthParser parser;
	private final OkexRequestBuilder builder;
	private final ObjectMapper mapper;
	private final CurrencyPair pair;
	
	public BuissnesWebSocketServiceImpl(CurrencyPair pair, DepthDistributor distributor) {
	   this.mapper = new ObjectMapper(); 
	   this.builder = new OkexRequestBuilder(mapper);
	   this.parser = new OkexDepthParser(mapper);
	   this.distributor = distributor;
	   this.pair = pair;
	}
	
	@Override
	public void onReceive(String msg){
		//Depth depth = parser.parseDepth(pair, msg)
		log.info("WebSocket Client received message: " + msg);
	
	}
}
