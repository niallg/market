package com.zuooh.market.okex;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.okex.test.BuissnesWebSocketServiceImpl;
import com.zuooh.market.okex.test.WebSoketClient;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthSubscriber;

@Slf4j
@Component
public class OkexDepthSubscriber implements DepthSubscriber {

   //private static final String DEPTH_URL = "wss://real.okex.com:10441/websocket";
   private static final String DEPTH_URL = "wss://real.okcoin.com:10440/websocket/okcoinapi";
   
   private final DepthDistributor distributor;
   private final OkexRequestBuilder builder;
   private final OkexDepthParser parser;
   private final ObjectMapper mapper;
   private final URI address;
   
   public OkexDepthSubscriber(DepthDistributor distributor) throws Exception {
      this.mapper = new ObjectMapper();
      this.builder = new OkexRequestBuilder(mapper);
      this.parser = new OkexDepthParser(mapper);
      this.address = new URI(DEPTH_URL);
      this.distributor = distributor;
   }
   
   @Override
   public Set<CurrencyPair> subscribe() {
//      String message = builder.createSubscription(pair);
//      
//      try {   
//         connect(pair, address);
//    
//         log.info("Successfully subscribed to: " + DEPTH_URL + " with [" + message + "]");
//      } catch(Exception e) {
//         throw new IllegalStateException("Could not subscribe for " + pair, e);
//      }
      return Collections.emptySet();
   }
   
   private void connect(CurrencyPair pair, URI address) {
      Currency base = pair.getBase();
      Currency quote = pair.getQuote();
      String token = String.format("ok_ethusd_depth",base, quote).toLowerCase();
      WebSocketService service = new BuissnesWebSocketServiceImpl(pair, distributor);
      WebSoketClient client = new WebSoketClient(DEPTH_URL, service);

      client.start();
      client.addChannel(token);

   }
}
