package com.zuooh.market.okex.test;


import com.zuooh.market.okex.WebSocketBase;
import com.zuooh.market.okex.WebSocketService;
/**
 * 通过继承WebSocketBase创建WebSocket客户端
 * @author okcoin
 *
 */
public class WebSoketClient extends WebSocketBase{
	public WebSoketClient(String url,WebSocketService service){
		super(url,service);
	}
}
