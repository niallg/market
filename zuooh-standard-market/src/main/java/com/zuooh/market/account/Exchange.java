package com.zuooh.market.account;

import java.util.List;

public interface Exchange {
   List<Balance> getBalances();
}
