package com.zuooh.market.account;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.zuooh.market.Currency;
import com.zuooh.market.Market;

public interface OrderRepository extends CrudRepository<Order, String> {
    List<Order> findByMarket(Market market);
    List<Order> findByBase(Currency base);
    List<Order> findByQuote(Currency quote);
}
