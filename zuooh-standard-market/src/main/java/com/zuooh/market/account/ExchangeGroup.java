package com.zuooh.market.account;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ExchangeGroup {

   private Optional<List<Exchange>> exchanges;
   
   public List<Balance> getBalances() {
      if(exchanges.isPresent()) {
         List<Exchange> list = exchanges.get();
         
         if(!list.isEmpty()) {
            List<Balance> all = new ArrayList<Balance>();
            
            for(Exchange exchange : list) {
               List<Balance> balance = exchange.getBalances();
               all.addAll(balance);
            }
            return all;
         }
      }
      return Collections.emptyList();
   }
}
