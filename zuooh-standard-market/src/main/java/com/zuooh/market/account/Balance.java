package com.zuooh.market.account;

import com.zuooh.market.Currency;
import com.zuooh.market.Market;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Balance {
   
   private Market market;
   private Currency currency;
   private double quantity;
}
