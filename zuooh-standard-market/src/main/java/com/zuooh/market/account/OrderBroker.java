package com.zuooh.market.account;

public interface OrderBroker {
   void execute(Order order);
}
