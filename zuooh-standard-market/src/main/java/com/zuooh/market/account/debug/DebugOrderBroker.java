package com.zuooh.market.account.debug;

import lombok.extern.slf4j.Slf4j;

import com.zuooh.market.account.Order;
import com.zuooh.market.account.OrderBroker;

@Slf4j
public class DebugOrderBroker implements OrderBroker {

   @Override
   public void execute(Order order) {
    
   }

}
