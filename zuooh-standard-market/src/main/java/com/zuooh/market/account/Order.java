package com.zuooh.market.account;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="orders")
public class Order implements Serializable {

   @Id
   @GeneratedValue(strategy=GenerationType.AUTO)
   private Long id;
   
   @Enumerated(EnumType.STRING)
   private OrderStatus status;
   
   @Enumerated(EnumType.STRING)
   private Currency base;
   
   @Enumerated(EnumType.STRING)
   private Currency quote;
   
   @Enumerated(EnumType.STRING)
   private Side side;
   
   @Enumerated(EnumType.STRING)
   private Market market;
   private String key; // unique order id on market
   private double quantity;
   private double price;
   private double stopPrice;
   private long time;
   
   public CurrencyPair getPair() {
      return new CurrencyPair(base, quote);
   }
}
