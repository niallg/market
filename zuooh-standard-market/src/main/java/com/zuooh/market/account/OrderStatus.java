package com.zuooh.market.account;

public enum OrderStatus {
   NEW,
   SENT,
   DONE,
   CANCEL
   
}
