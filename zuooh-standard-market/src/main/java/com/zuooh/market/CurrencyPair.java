package com.zuooh.market;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;

/**
 * A currency pair is the quotation and pricing structure of the currencies 
 * traded in the forex market; the value of a currency is a rate and is determined 
 * by its comparison to another currency. The first listed currency of a currency 
 * pair is called the base currency, and the second currency is called the quote 
 * currency. The currency pair indicates how much of the quote currency is needed 
 * to purchase one unit of the base currency.
 */
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class CurrencyPair implements Comparable<CurrencyPair> {
   
   public static CurrencyPair parsePair(String pair) {
      String[] parts = pair.toUpperCase().split("\\/"); // e.g XRP/USD
      
      if(parts.length != 2) {
         throw new IllegalStateException("Could not parse " + pair);
      }
      return new CurrencyPair(
            Currency.valueOf(parts[0]),
            Currency.valueOf(parts[1]));
   }
   
   private final Currency base;
   private final Currency quote;
   
   

   @Override
   public int compareTo(CurrencyPair other) {
      if(base != other.base) {
         return base.name().compareTo(other.base.name());
      }
      return quote.name().compareTo(other.quote.name());
   }
   
   public Currency getBase() {
      return base;
   }
   
   public Currency getQuote() {
      return quote;
   }
   
   @Override
   public String toString() {
      return String.format("%s/%s", base, quote);
   }
}
