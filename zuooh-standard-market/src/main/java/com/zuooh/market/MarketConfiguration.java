package com.zuooh.market;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.zuooh.market.account.Order;
import com.zuooh.market.account.OrderBroker;
import com.zuooh.market.account.OrderRepository;
import com.zuooh.market.fx.CurrencyConverter;
import com.zuooh.market.subscribe.DepthDistributor;

@EntityScan(basePackageClasses=Order.class)
@EnableJpaRepositories(basePackageClasses=OrderRepository.class)
@ComponentScan(basePackageClasses={DepthDistributor.class, OrderBroker.class, CurrencyConverter.class})
public class MarketConfiguration {

}
