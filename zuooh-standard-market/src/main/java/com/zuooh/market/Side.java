package com.zuooh.market;

import com.zuooh.market.subscribe.PriceComparator;

public enum Side {
   BID(true),
   ASK(false);
   
   private final PriceComparator comparator;
   
   private Side(boolean reverse) {
      this.comparator = new PriceComparator(reverse);
   }

   public PriceComparator getComparator(){
      return comparator;
   }
   
   public boolean isBid(){
      return this == BID;
   }
   
   public boolean isAsk() {
      return this == ASK;
   }
}
