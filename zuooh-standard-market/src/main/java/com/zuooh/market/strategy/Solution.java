package com.zuooh.market.strategy;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import com.zuooh.market.account.Order;

@Data
@Builder
@AllArgsConstructor
public class Solution {
   
   private List<Order> orders;
}
