package com.zuooh.market.fx;

public interface ExchangeRateListener {
   void onUpdate(ExchangeRate rate);
}
