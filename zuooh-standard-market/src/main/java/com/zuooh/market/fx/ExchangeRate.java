package com.zuooh.market.fx;

import lombok.AllArgsConstructor;

import com.zuooh.market.CurrencyPair;

@AllArgsConstructor
public class ExchangeRate {
   private final CurrencyPair pair;
   private final double rate;
}
