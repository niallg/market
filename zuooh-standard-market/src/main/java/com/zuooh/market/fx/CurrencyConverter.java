package com.zuooh.market.fx;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

import com.zuooh.market.Currency;

@Component
@AllArgsConstructor
public class CurrencyConverter {

   private final Optional<List<ExchangeRateSource>> sources;
   
   public double convert(double value, Currency from, Currency to) {
      if(from == to) {
         return value;
      }
      if(sources.isPresent()) {
         List<ExchangeRateSource> list = sources.get();
         
         for(ExchangeRateSource source : list) {
            Map<Currency, Double> rates = source.getRates(from);
            
            if(rates != null) {
               Double rate = rates.get(to);
               
               if(rate != null) {
                  return rate.doubleValue() * value;
               }
            }
         }
      }
      throw new IllegalStateException("Could not convert from " + from + " to " + to);
   }
}
