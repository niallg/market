package com.zuooh.market.fx;

import java.util.Map;

import com.zuooh.market.Currency;

public interface ExchangeRateSource {
   Map<Currency, Double> getRates(Currency base);
}
