package com.zuooh.market;

public enum Market {
   BINANCE,
   GDAX,
   BITFINEX,
   COINONE,
   COINEXCHANGE,
   KRAKEN,
   KUCOIN,
   YOBIT,
   BITSTAMP,
   BITHUMB,
   BITTREX,
   ALL
}
