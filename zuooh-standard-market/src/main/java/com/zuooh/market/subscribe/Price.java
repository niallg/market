package com.zuooh.market.subscribe;

import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Price {

   private final CurrencyPair pair;
   private final Market market;
   private final Side side;
   private final String key;
   private final double price;
   private final double quantity;

}
