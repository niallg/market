package com.zuooh.market.subscribe;

import java.util.Set;

import com.zuooh.market.CurrencyPair;

public interface DepthSubscriber {
	Set<CurrencyPair> subscribe();
}
