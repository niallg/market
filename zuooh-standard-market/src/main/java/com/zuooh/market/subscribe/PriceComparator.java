package com.zuooh.market.subscribe;

import java.util.Comparator;

public class PriceComparator implements Comparator<Price> {

   private final int multiplier;
   
   public PriceComparator() {
      this(false);
   }
   
   public PriceComparator(boolean reverse) {
      this.multiplier = reverse ? -1 : 1;
   }
   
   
   @Override
   public int compare(Price left, Price right) {
      double leftPrice = left.getPrice();
      double rightPrice = right.getPrice();
      
      if(leftPrice != rightPrice) {
         return multiplier * Double.compare(leftPrice, rightPrice);
      }
      double leftQuantity = left.getQuantity();
      double rightQuantity = right.getQuantity();
      
      return multiplier * Double.compare(leftQuantity, rightQuantity);
   }

}
