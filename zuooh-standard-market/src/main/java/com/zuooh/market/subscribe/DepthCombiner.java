package com.zuooh.market.subscribe;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.CurrencyType;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.fx.CurrencyConverter;

public class DepthCombiner {

   private final Map<CurrencyPair, DepthGroup> groups;
   private final Map<Currency, Double> rates; // estimate crypto to fiat exchange rates if needed
   private final CurrencyConverter converter;
   private final AtomicLong counter;
   private final Currency currency;
   private final Market market; // market with most accurate prices
   
   public DepthCombiner(CurrencyConverter converter, Currency currency, Market market){
      this.groups = new ConcurrentHashMap<CurrencyPair, DepthGroup>();
      this.rates = new ConcurrentHashMap<Currency, Double>();
      this.counter = new AtomicLong();
      this.converter = converter;
      this.currency = currency;
      this.market = market;
   }
   
   public synchronized Depth combine(Depth depth) {
      CurrencyPair pair = depth.getPair();
      Currency base = pair.getBase();
      Currency quote = pair.getQuote(); 
      double mid = 0;
      int count = 0;
      
      for(int i = 0; i < 2; i++) {
         Price bid = depth.getBids().getAt(i);
         Price ask = depth.getAsks().getAt(i);
       
         if(bid != null && ask != null) {
            mid += bid.getPrice();
            mid += ask.getPrice();
            count += 2;
         }
      }
      if(depth.getMarket().equals(market) && quote.equals(currency)) {
         rates.put(base, mid / count);
      }
      CurrencyPair key = new CurrencyPair(base, currency);
      
      if(!key.getQuote().equals(currency)) {
         throw new IllegalStateException("The key pair " + key + " must quote " + currency);
      }
      DepthGroup group = groups.get(key);
      
      if(group == null) {
         group = new DepthGroup(key);
         groups.put(key, group);
      }
      group.add(depth);
      return combine(group);
   }
   
   private synchronized Depth combine(DepthGroup group) {
      CurrencyPair pair = group.getPair();
      PriceSeries bids = combine(group, pair, Side.BID);
      PriceSeries asks = combine(group, pair, Side.ASK);
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();

      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, Market.ALL, version, time);
   }
   
   private synchronized PriceSeries combine(DepthGroup group, CurrencyPair key, Side side) {
      PriceComparator comparator = side.getComparator();
      List<Price> prices = group.getDepths()
            .stream()
            .filter(Objects::nonNull)
            .map(depth -> depth.getPrices(side))
            .flatMap(series -> series.getPrices().stream())
            .map(price -> convert(price, key))
            .sorted(comparator)
            .collect(Collectors.toList());
      
      return new PriceSeries(prices); 
   }
   
   private synchronized Price convert(Price price, CurrencyPair key) {
      Side side = price.getSide();
      String id = price.getKey();
      CurrencyPair pair = price.getPair();
      Currency quote = pair.getQuote(); // USD?
      Market market = price.getMarket();
      double quantity = price.getQuantity();
      double value = price.getPrice();
      double convert = convert(value, quote);
      
      if(!key.getQuote().equals(currency)) {
         throw new IllegalStateException("The key pair " + key + " must quote " + currency);
      }
      return new Price(key, market, side, id, convert, quantity);  
   }
   
   public synchronized double convert(double price, Currency from) {
      return convert(price, from, currency);
   } 
   
   public synchronized double convert(double price, Currency from, Currency to) {
      if(from.type == CurrencyType.FIAT) {
         return converter.convert(price, from, to);
      }
      Double rate = rates.get(from);

      if(rate != null) {
         return rate.doubleValue() * price;
      }
      return -1;
   }

   private class DepthGroup {
      
      private final Map<Market, Depth> markets;
      private final CurrencyPair pair;
      
      public DepthGroup(CurrencyPair pair) {
         this.markets = new ConcurrentHashMap<Market, Depth>();
         this.pair = pair;
      }
      
      public CurrencyPair getPair(){
         return pair;
      }
      
      public Collection<Depth> getDepths() {
         return markets.values();
      }
      
      public void add(Depth depth) {
         Market market = depth.getMarket();
         markets.put(market, depth);
      }
   }
}
