package com.zuooh.market.subscribe.poller;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;


@Slf4j
public abstract class PollerSubscriber {

   public final Poller poller;
   
   public PollerSubscriber(String address) {
      this.poller = new Poller(address);
   }
   
   public abstract void onResponse(String message);
   
   public void subscribe(){
      poller.start();
   }
   
   private class Poller implements Runnable {
      
      private final AtomicBoolean active;
      private final String address;
      
      public Poller(String address) {
         this.active = new AtomicBoolean();
         this.address = address;
      }
      
      public void start() {
         if(active.compareAndSet(false, true)) {
            Thread thread = new Thread(this);
            thread.start();
         }
      }
      
      @Override
      public void run() {
         try {
            long throttle = 0;
            
            while(active.get()) {
               try {
                  if(throttle > 0) {
                     Thread.sleep(throttle);
                  }
                  throttle = 60000;
                  URL url = new URL(address);
                  HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                  connection.setDoOutput(false);
                  connection.setRequestMethod("GET");
                  connection.setRequestProperty("Accept", "application/json");
                  InputStream stream = connection.getInputStream();
                  String text = IOUtils.toString(stream, "UTF-8");
                  onResponse(text);
               }catch(Exception e){
                  log.info("Could not poll", e);
                  active.set(false);
               }
            }
         } finally {
            active.set(false);
         }
      }
      
   }
}
