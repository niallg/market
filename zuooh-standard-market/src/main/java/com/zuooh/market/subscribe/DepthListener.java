package com.zuooh.market.subscribe;

import com.zuooh.market.Market;

public interface DepthListener {
	void onUpdate(Market market, Depth depth);
	void onError(Market market, Exception cause);
}
