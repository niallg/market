package com.zuooh.market.subscribe;

import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Depth {
   
   private final CurrencyPair pair;
   private final PriceSeries bids;
   private final PriceSeries asks;
   private final DepthType type;
   private final Market market;
   private final long version;
   private final long time;
   
   public PriceSeries getPrices(Side side) {
      if(side.isBid()) {
         return bids;
      }
      return asks;
   }
}
