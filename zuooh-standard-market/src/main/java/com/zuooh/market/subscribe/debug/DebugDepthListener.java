package com.zuooh.market.subscribe.debug;

import lombok.extern.slf4j.Slf4j;

import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.common.console.ConsoleTable;
import com.zuooh.market.common.console.ConsoleTableBuilder;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthListener;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
//@Component
public class DebugDepthListener implements DepthListener {

   @Override
   public void onUpdate(Market market, Depth depth) {
      ConsoleTable table = ConsoleTableBuilder.create("Level", "Sym", "Bid M", "Bid Q", "Bid", "Ask", "Ask Q", "Ask M", "Diff");
      CurrencyPair pair = depth.getPair();
      PriceSeries bids = depth.getBids();
      PriceSeries asks = depth.getAsks();
      
      for(int i = 0; i < 200; i++) {
         Object[] row = new Object[]{i + 1, pair, "", "", "", "", "", "", ""};
         Price bid = bids.getAt(i);
         Price ask = asks.getAt(i);
         
         if(ask != null || bid != null) {
            if(bid != null) {
               row[2] = bid.getMarket();
               row[3] = bid.getQuantity();
               row[4] = bid.getPrice();
            }
            if(ask != null) {
               row[5] = ask.getPrice();
               row[6] = ask.getQuantity();
               row[7] = ask.getMarket();
            }
            if(bid != null && ask != null) {
               row[8] = ask.getPrice() - bid.getPrice();
            }
            table.add(row);
         }
      }
      synchronized(System.err) {
         System.err.println();
         table.draw(System.err);
      }
   }

   @Override
   public void onError(Market market, Exception cause) {
      log.info("Got an error");
   }

}
