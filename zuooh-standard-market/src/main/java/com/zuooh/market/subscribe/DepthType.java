package com.zuooh.market.subscribe;

public enum DepthType {
   SNAPSHOT,
   UPDATE;
   
   public boolean isSnapshot(){
      return this == SNAPSHOT;
   }
   
   public boolean isUpdate() {
      return this == UPDATE;
   }
}
