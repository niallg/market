package com.zuooh.market.subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;

public class DepthMerger {

   private final Map<String, Depth> depths;
   
   public DepthMerger() {
      this.depths = new ConcurrentHashMap<String, Depth>();
   }
   
   public Depth merge(Depth depth) {
      CurrencyPair pair = depth.getPair();
      Market market = depth.getMarket();
      DepthType type = depth.getType();
      String key = String.format("%s:%s", market, pair);
      
      if(type.isUpdate()) {
         Depth original = depths.get(key);
         
         if(original != null) {
            depth = merge(depth, original);
         }
      } 
      depths.put(key, depth);
      return depth;
   }
   
   public Depth merge(Depth update, Depth original) {
      PriceSeries bids = merge(update, original, Side.BID);
      PriceSeries asks = merge(update, original, Side.ASK);
      CurrencyPair pair = update.getPair();
      Market market = update.getMarket();
      long version = update.getVersion();
      long time = update.getTime();
      
      return new Depth(pair, bids, asks, DepthType.SNAPSHOT, market, version, time);
   }
   
   public PriceSeries merge(Depth update, Depth original, Side side) {
      PriceSeries updatedPrices = update.getPrices(side);
      PriceSeries originalPrices = original.getPrices(side);
      int count = updatedPrices.size();
      
      if(count > 0) {
         List<Price> prices = new ArrayList<Price>();
         
         for(Price originalPrice: originalPrices) {
            Price merged = merge(updatedPrices, originalPrice);
            double mergedQuantity = merged.getQuantity();
            double mergedPrice = merged.getPrice();
            
            if(mergedQuantity > 0 && mergedPrice > 0) { // zeros mean clear price
               prices.add(merged);
            }
         }
         return new PriceSeries(prices);
      }
      return originalPrices;
   }
   
   public Price merge(PriceSeries update, Price price) {
      String key = price.getKey();
      
      for(Price next : update) {
         String match = next.getKey();
         
         if(match.equals(key)) {
            return next;
         }
      }
      return price;
   }
}
