package com.zuooh.market.subscribe;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuooh.market.CurrencyPair;

@Slf4j
@Component
public class DepthController  {

   private final Optional<List<DepthSubscriber>> subscribers;
   private final Set<CurrencyPair> pairs;
   
   @Autowired
   public DepthController(Optional<List<DepthSubscriber>> subscribers) {
      this.pairs = new CopyOnWriteArraySet<>();
      this.subscribers = subscribers;
   }
   
   public Set<CurrencyPair> getPairs() {
      return Collections.unmodifiableSet(pairs);
   }
   
   @PostConstruct
   public void start() {
      if(subscribers.isPresent()) {
         List<DepthSubscriber> list = subscribers.get();
         
         for(DepthSubscriber subscriber : list) {
            try {
               Set<CurrencyPair> done = subscriber.subscribe();
               pairs.addAll(done);
            }catch(Exception e) {
               log.info("Could not subscribe", e);
            }
         }
      }
   }
}
