package com.zuooh.market.subscribe;

import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.zuooh.market.Currency;
import com.zuooh.market.Market;
import com.zuooh.market.fx.CurrencyConverter;

@Slf4j
@Component
public class DepthDistributor implements DepthListener {

   private final Optional<List<DepthListener>> listeners;
   private final DepthCombiner combiner;
   private final DepthMerger merger;
   
   public DepthDistributor(CurrencyConverter converter, Optional<List<DepthListener>> listeners) {
      this.combiner = new DepthCombiner(converter, Currency.USD, Market.KRAKEN);
      this.merger = new DepthMerger();
      this.listeners = listeners;
   }
   
   public double getConversion(double price, Currency from) {
      try {
         return combiner.convert(price, from);
      } catch(Exception e) {
         log.info("Could not get conversion for " + from, e);
      }
      return -1;
   } 
   
   public double getConversion(double price, Currency from, Currency to) {
      try {
         return combiner.convert(price, from, to);
      } catch(Exception e) {
         log.info("Could not get conversion for " + from + "/" + to, e);
      }
      return -1;
   }
   
   @Override
   public void onUpdate(Market market, Depth depth) {
      if(listeners.isPresent()) {
         List<DepthListener> list = listeners.get();
         Depth merged = merger.merge(depth);
         Depth combined = combiner.combine(merged);
         
         for(DepthListener listener : list) {
            try {
               listener.onUpdate(market, combined);
            } catch(Exception e) {
               log.info("Could not update listener", e);
            }
         }
      }
   }

   @Override
   public void onError(Market market, Exception cause) {
      if(listeners.isPresent()) {
         List<DepthListener> list = listeners.get();
         
         for(DepthListener listener : list) {
            try {
               listener.onError(market, cause);
            } catch(Exception e) {
               log.info("Could not update listener", e);
            }
         }
      }
   }

}
