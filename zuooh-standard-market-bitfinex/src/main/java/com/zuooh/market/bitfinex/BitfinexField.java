package com.zuooh.market.bitfinex;

public interface BitfinexField {

   String SUBSCRIBE = "subscribe";
   String BOOK = "book";
   String R0 = "R0";

   /*
      https://docs.bitfinex.com/v2/reference#ws-public-raw-order-books
    
      ORDER_ID int   Order id
      PRICE float Order price; if 0 you have to remove the order from your book
      �AMOUNT  float Total amount available at that price level.
    */
   interface OrderField {
      int ORDER_ID = 0;
      int PRICE = 1;
      int AMOUNT = 2;
   }

}
