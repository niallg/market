package com.zuooh.market.bitfinex;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=BitfinexProperties.class)
public class BitfinexConfiguration {
}
