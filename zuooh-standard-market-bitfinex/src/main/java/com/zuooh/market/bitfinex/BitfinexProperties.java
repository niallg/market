package com.zuooh.market.bitfinex;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zuooh.market.CurrencyPair;

@Component
public class BitfinexProperties {

   @Value("${bitfinex.pairs}")
   private String[] pairs;
   
   @Value("${bitfinex.charges}")
   private double charges;
   
   @Value("${bitfinex.enabled:true}")
   private boolean enabled;
  
   public boolean isEnabled() {
      return enabled;
   }
   
   public double getCharges() {
      return charges;
   }
   
   public Set<CurrencyPair> getPairs() {
      return Arrays.asList(pairs)
            .stream()
            .filter(Objects::nonNull)
            .map(pair -> CurrencyPair.parsePair(pair))
            .collect(Collectors.toSet());
   }
  
}
