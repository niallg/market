package com.zuooh.market.bitfinex;

import static com.zuooh.market.bitfinex.BitfinexField.BOOK;
import static com.zuooh.market.bitfinex.BitfinexField.R0;
import static com.zuooh.market.bitfinex.BitfinexField.SUBSCRIBE;
import lombok.AllArgsConstructor;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;

public class BitfinexRequestBuilder {

   private final PrettyPrinter printer;
   private final ObjectMapper mapper;
   
   public BitfinexRequestBuilder(ObjectMapper mapper) {
      this.printer = new DefaultPrettyPrinter();
      this.mapper = mapper;
   }
   
   /*
      {
         "event":"subscribe",
         "channel":"book",
         "pair":"<PAIR>",
         "prec":"R0"
      } 
    */
   public String createSubscription(CurrencyPair pair) {
      try {
         Currency base = pair.getBase();
         Currency quote = pair.getQuote();
         String token = String.format("%s%s",base, quote);
         SubscribeRequest request = new SubscribeRequest(SUBSCRIBE, BOOK, token, R0, null);
         return mapper.writer(printer).writeValueAsString(request);
      } catch(Exception e) {
         throw new IllegalStateException("Could not create subscription", e);
      }
   }
   
   @Data
   @AllArgsConstructor
   @JsonInclude(Include.NON_NULL)
   private static class SubscribeRequest {
      
      private String event;
      private String channel;
      private String pair;
      private String prec;
      private String freq;
   }
}
