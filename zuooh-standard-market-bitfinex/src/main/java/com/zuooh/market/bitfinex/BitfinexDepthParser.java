package com.zuooh.market.bitfinex;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
public class BitfinexDepthParser {

   private final ObjectMapper mapper;
   private final AtomicLong counter;

   public BitfinexDepthParser(ObjectMapper mapper) {
      this.counter = new AtomicLong();
      this.mapper = mapper;
   }
   
   
   /*
     [
        33904,
        [
          6247536191,
          0,
          -1
        ]
      ]
    */
   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         Object message = mapper.readValue(source, Object.class);
         
         if(List.class.isInstance(message)) {
            List list = getListField((List)message, 1);
            
            if(list != null) {
               return parseOrderBook(pair, list);
            }
         }
         if(log.isTraceEnabled()) {
            log.trace("Received message: " + source);
         }
      } catch (Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
      return null;
   }
   
   private Depth parseOrderBook(CurrencyPair pair, List<?> orders) {
      List<Price> prices = parseActiveOrders(pair, orders);
      PriceSeries bids = parsePriceList(pair, Side.BID, prices);
      PriceSeries asks = parsePriceList(pair, Side.ASK, prices);
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bids, asks, DepthType.UPDATE, Market.BITFINEX, version, time);
   }
   
   private PriceSeries parsePriceList(CurrencyPair pair, Side side, List<Price> prices) {
     List<Price> matches = prices.stream()
           .filter(price -> price.getPair().equals(pair))
           .filter(price -> price.getSide().equals(side))
           .collect(Collectors.toList());
     
     return new PriceSeries(matches);
   }
   
   private List<Price> parseActiveOrders(CurrencyPair pair, List<?> orders) {
      return orders.stream()
            .filter(Objects::nonNull)
            .filter(entry -> entry instanceof List)
            .map(entry -> new OrderBookResponse(pair, (List)entry))
            .map(entry -> parseActiveOrder(entry))
            .collect(Collectors.toList());
   }
   
   private Price parseActiveOrder(OrderBookResponse response) {
      Side side = parseSide(response);
      CurrencyPair pair = response.getPair();
      String order = response.getOrderId();
      double price = response.getPrice();
      double amount = response.getAmount();
      
      return new Price(pair, Market.BITFINEX, side, order, Math.abs(price), Math.abs(amount));
   }

   private Side parseSide(OrderBookResponse response) {
      double quantity = response.getAmount();
      return quantity >= 0 ? Side.BID : Side.ASK; 
   }
   
   private static List getListField(List list, int index) {
      Object value = list.get(index);
      
      if(List.class.isInstance(value)) {
         return (List)value;
      }
      return null;
   }
   
   private static Long getLongField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Long.parseLong(value);
      }
      return null;
   }

   private static Double getDoubleField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Double.parseDouble(value);
      }
      return null;
   }

   private static String getField(List list, int index) {
      Object value = list.get(index);

      if (value != null) {
         return String.valueOf(value);
      }
      return null;
   }
   
   @AllArgsConstructor
   private class OrderBookResponse {

      private final CurrencyPair pair;
      private final List<?> list;
      
      public CurrencyPair getPair(){
         return pair;
      }

      public String getOrderId() {
         return getField(list, BitfinexField.OrderField.ORDER_ID);
      }
      
      public Double getAmount() {
         return getDoubleField(list, BitfinexField.OrderField.AMOUNT);
      }

      public Double getPrice() {
         return getDoubleField(list, BitfinexField.OrderField.PRICE);
      }
   }

}
