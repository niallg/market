package com.zuooh.market.bitfinex;

import java.io.Closeable;
import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import lombok.extern.slf4j.Slf4j;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthDistributor;
import com.zuooh.market.subscribe.DepthListener;
import com.zuooh.market.subscribe.DepthSubscriber;

@Slf4j
@Component
public class BitfinexDepthSubscriber implements DepthSubscriber {

   private static final String DEPTH_URL = "wss://api.bitfinex.com/ws/2";
   
   private final Map<CurrencyPair, WebSocketSubscriber> sockets;
   private final DepthDistributor distributor;
   private final BitfinexRequestBuilder builder;
   private final BitfinexDepthParser parser;
   private final BitfinexProperties properties;
   private final ObjectMapper mapper;
   private final URI address;
   
   public BitfinexDepthSubscriber(BitfinexProperties properties, DepthDistributor distributor) throws Exception {
      this.sockets = new ConcurrentHashMap<CurrencyPair, WebSocketSubscriber>();
      this.mapper = new ObjectMapper();
      this.builder = new BitfinexRequestBuilder(mapper);
      this.parser = new BitfinexDepthParser(mapper);
      this.address = new URI(DEPTH_URL);
      this.properties = properties;
      this.distributor = distributor;
   }
   
   @Override
   public Set<CurrencyPair> subscribe() {
      if(properties.isEnabled()) {
         Set<CurrencyPair> pairs = properties.getPairs();
         pairs.stream().forEach(this::subscribe);
         return pairs;
      }
      return Collections.emptySet();
   }
   
   private void subscribe(CurrencyPair pair) {
      String message = builder.createSubscription(pair);
      
      try {   
         WebSocketSubscriber subscriber = connect(pair, address);
    
         log.info("Successfully subscribed to: " + DEPTH_URL + " with [" + message + "]");
         subscriber.sendSubscription(message);
      } catch(Exception e) {
         throw new IllegalStateException("Could not subscribe for " + pair, e);
      }
   }
   
   private WebSocketSubscriber connect(CurrencyPair pair, URI address) {
      WebSocketSubscriber subscriber = sockets.get(pair);
      
      if(subscriber == null || subscriber.isClosed()) {
         try {
            subscriber = new WebSocketSubscriber(distributor, pair, address);
            
            subscriber.connect();
            sockets.put(pair, subscriber);
         } catch(Exception e) {
            throw new IllegalStateException("Could not subscribe to " + address, e);
         }
      }
      return subscriber;
   }

   private class WebSocketSubscriber extends WebSocketClient implements Closeable{

      private final BlockingQueue<String> messages;
      private final DepthListener listener;
      private final CurrencyPair pair;
      
      public WebSocketSubscriber(DepthListener listener, CurrencyPair pair, URI address) {
         super(address);
         this.messages = new LinkedBlockingQueue<String>();
         this.listener = listener;
         this.pair = pair;
      }
      
      public synchronized void sendSubscription(String message) {
         messages.offer(message);
         flushMessages();
      }

      private void flushMessages() {
         if(isOpen()) {
            while(!messages.isEmpty()) {
               String message = messages.peek();
               
               try{
                  send(message);
                  messages.poll();
               }catch(Exception e){
                  log.info("Could not send subscription", e);
                  break;
               }
            }
         }
      }
      
      @Override
      public void onOpen(ServerHandshake handshakedata) {
         log.info("onOpen()");
         flushMessages();
      }

      @Override
      public void onMessage(String message) {
         Depth depth = parser.parseDepth(pair, message);
         
         if(depth != null) {
            listener.onUpdate(Market.BITFINEX, depth);
         }
         flushMessages();
      }

      @Override
      public void onClose(int code, String reason, boolean remote) {
         log.info("onClose(" + code + ", " + reason + ")");
         sockets.remove(pair);
      }

      @Override
      public void onError(Exception cause) {  
         log.info("onError()", cause);
         listener.onError(Market.BITFINEX, cause);
      }
   }
}
