   	
   var tables = {};
   var headers = {};
   var subscription= {};
   var routes= {};
   var disconnect = [];
   var socket = null;
   var connections = 0;
   var attempts = 0;
   var total = 1;

createRoute("DEPTH", updateDepth, null);
startSocket();
pollHash();
updateRates();

function updateRates() {
	$.get("/rates?base=USD", function( data ) {
	   	var element = document.getElementById("rates");
   		var style = "style='border: 1px solid black;'";
	   	var details = JSON.parse(data);
		var table = "";

	   	table += "<table border='0' cellspacing='0' cellpadding='0'>";
		//table += "<th>Pair</th>";
		table += "<th " + style + ">Currency</th>";
		table += "<th " + style + ">Rate</th>";
		
		for (var currency in details) {
		    if (details.hasOwnProperty(currency)) {  
		    	var value = details[currency];
		    	
		    	table += '<tr>';
		    	table += '<td ' + style + '>&nbsp;USD/' + currency+ '&nbsp;</td>';
		    	table += '<td ' + style + '>&nbsp;' + value+ '&nbsp;</td>';
		    	table += '</tr>';
		    }
		}
		table += '</table>';
		element.innerHTML = table;
	});
}

function getQueryString(field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
}

function pollHash() {
	setInterval(function() {
	   	var element = document.getElementById("depth");
    	var header = document.getElementById("pair");
	   	var hash = window.location.hash;
	   	
	   	if(element && hash) {
	   		var key = hash.toLowerCase().substring(1);
	   		var table = tables[key];
	   		var title = headers[key];
	   		
	   		if(table != null) {
		    	element.innerHTML = table;
	   		} else {
	   			element.innerHTML = "";
	   		}
	   		if(title != null) {
	   			header.innerHTML = title;
	   		} else {
	   			header.innerHTML = "";
	   		}
	   	}
	}, 500);
}

function isCryptoCurrency(currency) {
	return currency == "ETH" || currency == "BTC" || currency == "XRP";
}

function updateDepth(socket, type, text) {
   	var message = JSON.parse(text);
   	var key = (message.pair.base +"_" + message.pair.quote).toLowerCase();
   	var element = document.getElementById("depth");
   	var hash = window.location.hash;
   	var rounding = 2;
   	var realPrice = null;
   	var filter = key;
   	
   	if(isCryptoCurrency(message.pair.quote)) {
   		rounding = 10;
   	}
   	if(hash) {
   		filter = hash.substring(1);
   	}
   	if(element != null) {
   		var style = "style='border: 1px solid black;'";
	   	var table = "";

	   	table += "<table border='0' cellspacing='0' cellpadding='0'>";
		//table += "<th>Pair</th>";
		table += "<th " + style + ">Bid M</th>";
		table += "<th " + style + ">Bid Q</th>";
		table += "<th " + style + ">Bid</th>";
		table += "<th " + style + ">Ask</th>";
		table += "<th " + style + ">Ask Q</th>";
		table += "<th " + style + ">Ask M</th>";
		table += "<th " + style + ">Spread</th>";	
		table += "<th " + style + ">Percent</th>";	
	   		
	   	for(var i = 0; i < 200; i++) {
	   		var bid = message.bids.prices[i];
	   		var ask = message.asks.prices[i];
	   		
	   		if(bid && ask) {
		   		var spread = parseFloat(ask.price - bid.price).toFixed(4);
		   		var max = Math.max(ask.price, bid.price);
		   		var percentage = parseFloat((spread / max) * 100).toFixed(2);
		   		var bidStyle = style;
		   		var askStyle = style;
		   		var color = style;
		   		
		   		if(spread <0){
		   			color = "style='border: 1px solid black; background-color: #ff6961;'";
		   		} else {
		   			if(!realPrice){
		   				var mid = document.getElementById(key + "_price");
		   				realPrice = parseFloat((bid.price + ask.price) / 2).toFixed(rounding);
		   				
		   				if(mid) {
		   					mid.innerHTML = '&nbsp;' + realPrice + '&nbsp;';
		   				}
		   			}
		   		}
		   		if(bid.quantity * bid.price > 20000) {
		   			bidStyle = "style='border: 1px solid black; background-color: #77dd77;'";
		   		}
		   		if(ask.quantity * ask.price > 20000) {
		   			askStyle = "style='border: 1px solid black; background-color: #77dd77;'";
		   		}
		   	   	table += "<tr>";
		   		//table += "<td>&nbsp;" + message.pair.base +"/" + message.pair.quote + "&nbsp;</td>";
		   		table += "<td " + bidStyle + ">&nbsp;" + bid.market + "&nbsp;</td>";
		   		table += "<td " + bidStyle + ">&nbsp;" + bid.quantity + "&nbsp;</td>";
		   		table += "<td " + bidStyle + ">&nbsp;" + parseFloat(bid.price).toFixed(rounding) + "&nbsp;</td>";
		   		table += "<td " + askStyle + ">&nbsp;" + parseFloat(ask.price).toFixed(rounding) + "&nbsp;</td>";
		   		table += "<td " + askStyle + ">&nbsp;" + ask.quantity + "&nbsp;</td>";
		   		table += "<td " + askStyle + ">&nbsp;" + ask.market + "&nbsp;</td>";
		   		table += "<td " + color + ">&nbsp;" + spread + "&nbsp;</td>";
		   		table += "<td " + color + ">&nbsp;" + percentage + "%&nbsp;</td>";
		   	   	table += "</tr>";
	   		}
	   	}
	   	var title = key.toUpperCase().split("_").join("/");
	   	
	   	table += "</table>";
	   	tables[key] = table;
	   	headers[key] = title;
	   	
	    if(key == filter){
	    	var header = document.getElementById("pair");
	    	
	    	header.innerHTML = title;
	    	element.innerHTML = table;
	    }
   	}
}

   
   function startSocket() {
      createSubscription();
      openSocket(); /* delay connect */
   }
   
   function refreshSocket() {
      if(socket) {
         socket.close();
      }
   }
   
   function isSocketOpen() {
      if(socket){
         return socket.readyState == 1;
      }
      return false;
   }
   
   function sendEvent(type, message) {
      if(message) {
         var payload = JSON.stringify(message);
         socket.send(type + ":" + payload);
      } else {
         socket.send(type);
      }
   }
   
   function createSubscription() {
      var host = window.document.location.hostname;
      var port = window.document.location.port;
      var scheme = window.document.location.protocol;
      var path = window.document.location.pathname;
      var query = window.document.location.search;
   
      subscription['panic'] = false;
      subscription['query'] = query;
   
      var address = "ws://";
   
      if (scheme.indexOf("https") == 0) {
         address = "wss://"
      }
      address += host;
      
      if((port - parseFloat(port) + 1) >= 0) {
         address += ":";
         address += port;
      }   
      var segments = path.split("/");
   
      if (segments.length > 2) {
         address += "/connect/" + segments[2];
      } else {
         address += "/connect"
      }   
      address += query;
   
      subscription['address'] = address;
   }
   
   function disableRoutes() {
      if (subscription.panic == false) {
         subscription['panic'] = true;
         socket.close();      
      }
   }
   
   function enableRoutes() {
      if (subscription.panic == true) {
         subscription['panic'] = false;   
         socket.close();
      }
   }
   
   function openSocket() {
      socket = new WebSocket(subscription.address);
   
      socket.onopen = function() {
         attempts = 1;
         connections++;
         //LoadSpinner.hide(); // on hide overlay
         console.log("Socket connected to '" + subscription.address + "'");
      };
   
      socket.onerror = function(message) {
         var length = disconnect.length;
         
         for(var i = 0; i < length; i++) {
            var callback = disconnect[i];
            
            if(callback != null) {
               callback(); // disconnected
            }
         }
         console.log("Error connecting to '" + subscription.address + "'");
      };
   
      socket.onclose = function(message) {
         var exponent = Math.pow(2, attempts++);
         var interval = (exponent - 1) * 1000;
         var length = disconnect.length;
         var reference = openSocket();
   
         if (interval > 30 * 1000) {
            interval = 30 * 1000;
         }
         setTimeout(reference, interval);
         
         for(var i = 0; i < length; i++) {
            var callback = disconnect[i];
            
            if(callback != null) {
               callback(); // disconnected
            }
         }
        
         console.log("Connection closed to '" + subscription.address + "' reconnecting in " + interval + " ms");
      };
   
      socket.onmessage = function(message) {
         var data = message.data;
         var index = data.indexOf(':');
         var value = null;
         var type = null;
         
         if(index != -1) {
            value = data.substring(index + 1);
            type = data.substring(0, index);
         } else {
            type = data;
         }
         var route = routes[type];
         
         if(route != undefined) {
            if(!subscription.panic) { // this might cause problems with missing messages
               for(var i = 0; i < route.length; i++) {
                  var func = route[i];
                  
                  if(func != null) {
                     func(this, type, value);
                  }
               }
            }
         } else {
            console.log("No route defined for '" + type+ "' with '" + value + "'");
         }
         //LoadSpinner.hide(); // hide the spinner
      };
   }
   
   function createRoute(code, method, failure) {
      var name = code.toUpperCase();
      
      if(name != code) {
         alert("Illegal route '" + name + "' is not in upper case");
      }
      var route = routes[code];
      
      if(route == null) {
         route = [];
         routes[code] = route;
      }
      if(method != null) {
         var length = route.length;
         var exists = false;
         
         for(var i = 0; i < length; i++) {
            var callback = route[i];
            
            if(callback == method) { // don't add twice
               exists = true;
            }
         }
         if(!exists) {
            route.push(method); // add a route listener
         }
      }
      createTermination(failure);
      refreshSocket();
   }
   
   function createTermination(failure) {
      if(failure != null) {
         var length = disconnect.length;
         var exists = false;
         
         for(var i = 0; i < length; i++) {
            var callback = disconnect[i];
            
            if(callback == failure) { // don't add twice
               exists = true;
            }
         }
         if(!exists) {
            disconnect.push(failure); // add a disconnect listener
         }
      }
   }