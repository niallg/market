CREATE TABLE orders (
  id BIGINT auto_increment,
  key VARCHAR(255),
  status VARCHAR(20),
  base VARCHAR(5),
  quote VARCHAR(5),  
  side VARCHAR(5),
  market VARCHAR(40),
  quantity DOUBLE,
  price DOUBLE,
  stop_price DOUBLE,
  time BIGINT,
  PRIMARY KEY(id)
);

