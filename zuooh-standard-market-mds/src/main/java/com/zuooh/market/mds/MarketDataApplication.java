package com.zuooh.market.mds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.zuooh.market.binance.EnableBinance;
import com.zuooh.market.bitfinex.EnableBitfinex;
import com.zuooh.market.bithumb.EnableBithumb;
import com.zuooh.market.bitstamp.EnableBitstamp;
import com.zuooh.market.bittrex.EnableBittrex;
import com.zuooh.market.coinexchange.EnableCoinexchange;
import com.zuooh.market.common.manage.EnableManage;
import com.zuooh.market.ecb.EnableEcb;
import com.zuooh.market.gdax.EnableGdax;
import com.zuooh.market.kraken.EnableKraken;
import com.zuooh.market.kucoin.EnableKucoin;
import com.zuooh.market.mds.server.EnableStreaming;

@EnableEcb
@EnableBinance
@EnableBitfinex
@EnableGdax
@EnableKraken
@EnableBitstamp
@EnableBithumb
@EnableBittrex
@EnableCoinexchange
@EnableKucoin
@EnableStreaming
@EnableManage
@EnableAutoConfiguration
@SpringBootApplication
@EnableConfigurationProperties
public class MarketDataApplication {

   // ConfigFileApplicationListener
   // spring.config.location=file:etc/ ----> default is classpath:/,classpath:/config/,file:./,file:./config/
   // 
   public static void main(String[] list) {
      SpringApplication.run(MarketDataApplication.class, list);
   }
}
