package com.zuooh.market.mds.server;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=StreamServer.class)
public class StreamConfiguration {

}
