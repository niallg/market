package com.zuooh.market.mds.server;

import java.io.PrintStream;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.Market;
import com.zuooh.market.account.Balance;
import com.zuooh.market.account.ExchangeGroup;
import com.zuooh.market.subscribe.DepthDistributor;

@Component
public class BalancesResource {

   private final DepthDistributor distributor;
   private final ExchangeGroup group;
   private final ObjectMapper mapper;
   
   public BalancesResource(ExchangeGroup group, DepthDistributor distributor) {
      this.mapper = new ObjectMapper();
      this.distributor = distributor;
      this.group = group;
   }
   
   @SneakyThrows
   public void handle(Request req, Response resp) {
      List<Balance> balances = group.getBalances();
      PrintStream stream = resp.getPrintStream();
      List<BalanceAndValue> list = balances.stream()
            .filter(Objects::nonNull)
            .map(balance -> {
               Market market = balance.getMarket();
               Currency currency = balance.getCurrency();
               double quantity = balance.getQuantity();
               double conversion = distributor.getConversion(1, currency);
               
               return new BalanceAndValue(market, currency, quantity, conversion);
            })
            .collect(Collectors.toList());
      String text = mapper.writeValueAsString(list);
      
      resp.setStatus(Status.OK);
      resp.setContentType("application/json");
      stream.println(text);
      stream.close();
   }
   
   @Data
   @Builder
   @NoArgsConstructor
   @AllArgsConstructor
   private static class BalanceAndValue {
      
      private Market market;
      private Currency currency;
      private double quantity;
      private double value;
      
   }
}
