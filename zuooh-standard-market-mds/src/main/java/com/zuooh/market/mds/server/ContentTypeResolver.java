package com.zuooh.market.mds.server;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.zuooh.market.common.ClassPathReader;

@Component
public class ContentTypeResolver {

   private static final String TYPES_FILE = "types.json";

   private final Map<String, String> cache;
   private final Map<String, String> types;
   private final Gson gson;

   public ContentTypeResolver() {
      this.cache = new ConcurrentHashMap<String, String>();
      this.types = new ConcurrentHashMap<String, String>();
      this.gson = new Gson();
   }

   private Map<String, String> readTypes() {
      if (types.isEmpty()) {
         try {
            String text = ClassPathReader.load(TYPES_FILE);
            Map<String, String> map = (Map) gson.fromJson(text, Map.class);

            types.putAll(map);
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
      return types;
   }

   public String matchPath(String path) {
      Map<String, String> types = readTypes();
      Set<String> expressions = types.keySet();
      String token = path.toLowerCase();

      for (String expression : expressions) {
         if (token.equalsIgnoreCase(expression) || token.matches(expression)) {
            String type = types.get(expression);

            if (type != null) {
               return type;
            }
         }
      }
      return "application/octet-stream";
   }

   public String resolveType(String path) {
      String result = cache.get(path);

      if (result == null) {
         String type = matchPath(path);

         if (type != null) {
            cache.put(path, type);
            return type;
         }
      }
      return result;
   }
}