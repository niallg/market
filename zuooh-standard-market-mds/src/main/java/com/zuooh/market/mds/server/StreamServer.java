package com.zuooh.market.mds.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;

import lombok.AllArgsConstructor;

import org.simpleframework.http.Path;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerSocketProcessor;
import org.simpleframework.http.socket.service.DirectRouter;
import org.simpleframework.http.socket.service.RouterContainer;
import org.simpleframework.transport.SocketProcessor;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;
import org.springframework.stereotype.Component;

import com.zuooh.market.common.ClassPathReader;

@Component
@AllArgsConstructor
public class StreamServer implements Container {

   private final ExchangeRateResource rates;
   private final CurrencyPairResource pairs;
   private final BalancesResource balances;
   private final ContentTypeResolver resolver;
   private final StreamContainer container;

   @Override
   public void handle(Request req, Response resp) {
      try {
         Path path = req.getPath();
         String normal = path.getPath();
         
         if(normal.equals("/status")) {
            PrintStream out = resp.getPrintStream();
            resp.setStatus(Status.OK);
            resp.setContentType("text/plain");
            out.println("Everything is ok");
            out.close();
         }else {
            normal = path.getPath(1);
            
            if(normal.startsWith("/rates")) {
               rates.handle(req, resp);
            } else if(normal.startsWith("/pairs")) {
               pairs.handle(req, resp);
            } else if(normal.startsWith("/balances")) {
               balances.handle(req, resp);
            } else {
               String type = resolver.resolveType(normal);
               String text = ClassPathReader.load(normal);
               PrintStream out = resp.getPrintStream();
               resp.setStatus(Status.OK);
               resp.setContentType(type);
               out.println(text);
               out.close();
            }
         }
      } catch (Exception e) {
         try {
            resp.setStatus(Status.INTERNAL_SERVER_ERROR);
            resp.close();
         } catch (Throwable ex) {
         }
      }
   }

   @PostConstruct
   public void start() throws IOException {
      DirectRouter router = new DirectRouter(container);
      RouterContainer rout = new RouterContainer(this, router, 2);
      SocketProcessor processor = new ContainerSocketProcessor(rout);
      Connection conn = new SocketConnection(processor);
      conn.connect(new InetSocketAddress(9091));
   }

}
