package com.zuooh.market.mds.server;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArraySet;

import lombok.extern.slf4j.Slf4j;

import org.simpleframework.http.Path;
import org.simpleframework.http.Request;
import org.simpleframework.http.socket.FrameChannel;
import org.simpleframework.http.socket.Session;
import org.simpleframework.http.socket.service.Service;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthListener;

@Slf4j
@Component
public class StreamContainer implements Service, DepthListener {

   private final Map<CurrencyPair, Depth> depths;
   private final Set<FrameChannel> channels;
   private final ObjectMapper mapper;
   
   public StreamContainer(){
      this.depths = new TreeMap<CurrencyPair, Depth>();
      this.channels = new CopyOnWriteArraySet<>();
      this.mapper = new ObjectMapper();
   }
   
   @Override
   public synchronized void onUpdate(Market market, Depth depth) {
      try {
         CurrencyPair pair = depth.getPair();
         String text = mapper.writeValueAsString(depth);
       
         if(pair != null) {
            depths.put(pair, depth);
         }
         for(FrameChannel channel : channels) {
            try{
               channel.send("DEPTH:"+text);
            } catch(Exception e) {
               channels.remove(channel);
               log.info("Could not send frame", e);
            }
         }
      }catch(Exception e){
         log.info("Could not write depth", e);;
      }
   }
   
   private synchronized void onConnect(FrameChannel channel) {
      try{
         Set<Entry<CurrencyPair, Depth>> entries = depths.entrySet();
         
         for(Entry<CurrencyPair, Depth> entry : entries) {
            Depth depth = entry.getValue();
            CurrencyPair pair = entry.getKey();
            String text = mapper.writeValueAsString(depth);
            
            log.info("Sending: " + pair);
            channel.send("DEPTH:"+text);
         }
         for(Entry<CurrencyPair, Depth> entry : entries) {
            Depth depth = entry.getValue();
            CurrencyPair pair = entry.getKey();
            String text = mapper.writeValueAsString(depth);
            
            log.info("Sending: " + pair);
            channel.send("DEPTH:"+text);
         }
      } catch(Exception e) {
         channels.remove(channel);
         log.info("Could not send frame", e);
      }
   }

   @Override
   public synchronized void onError(Market market, Exception cause) {
      log.info("Error processing depth", cause);
   }

   
   @Override
   public synchronized void connect(Session connection) {
      Request request = connection.getRequest();    
      Path path = request.getPath(); // /connect/<project-name>
      
      try {
         FrameChannel channel = connection.getChannel();
         channels.add(channel);
         onConnect(channel);
      }catch(Exception e){
         log.info("Error connecting " + path, e);
      }
      
   }

}
