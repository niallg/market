package com.zuooh.market.mds.server;

import java.io.PrintStream;
import java.util.Map;

import lombok.SneakyThrows;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.fx.ExchangeRateSource;

@Component
public class ExchangeRateResource {

   private final ExchangeRateSource source;
   private final ObjectMapper mapper;
   
   public ExchangeRateResource(ExchangeRateSource source) {
      this.mapper = new ObjectMapper();
      this.source = source;
   }
   
   @SneakyThrows
   public void handle(Request req, Response resp) {
      String base = req.getParameter("base");
      Currency currency = Currency.valueOf(base.toUpperCase());
      Map<Currency, Double> rates = source.getRates(currency);
      String text = mapper.writeValueAsString(rates);
      PrintStream stream = resp.getPrintStream();
      
      resp.setStatus(Status.OK);
      resp.setContentType("application/json");
      stream.println(text);
      stream.close();
   }
}
