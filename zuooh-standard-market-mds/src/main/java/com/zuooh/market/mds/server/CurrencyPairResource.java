package com.zuooh.market.mds.server;

import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;

import lombok.SneakyThrows;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.subscribe.DepthController;

@Component
public class CurrencyPairResource {

   private final DepthController controller;
   private final ObjectMapper mapper;
   
   public CurrencyPairResource(DepthController controller) {
      this.mapper = new ObjectMapper();
      this.controller = controller;
   }
   
   @SneakyThrows
   public void handle(Request req, Response resp) {
      String base = req.getParameter("base");
      Currency currency = Currency.valueOf(base.toUpperCase());
      Set<CurrencyPair> pairs = controller.getPairs();
      Set<CurrencyPair> modified = new TreeSet<CurrencyPair>();
      
      for(CurrencyPair pair : pairs) {
         modified.add(new CurrencyPair(pair.getBase(), currency));
      }
      String text = mapper.writeValueAsString(modified);
      PrintStream stream = resp.getPrintStream();
      
      resp.setStatus(Status.OK);
      resp.setContentType("application/json");
      stream.println(text);
      stream.close();
   }
}
