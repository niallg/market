package com.zuooh.market.kraken;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=KrakenProperties.class)
public class KrakenConfiguration {

}
