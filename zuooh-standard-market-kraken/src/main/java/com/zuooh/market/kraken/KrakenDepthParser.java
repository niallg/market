package com.zuooh.market.kraken;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.CurrencyPair;
import com.zuooh.market.Market;
import com.zuooh.market.Side;
import com.zuooh.market.subscribe.Depth;
import com.zuooh.market.subscribe.DepthType;
import com.zuooh.market.subscribe.Price;
import com.zuooh.market.subscribe.PriceSeries;

@Slf4j
public class KrakenDepthParser {
   
   private final ObjectMapper mapper;
   private final AtomicLong counter;

   public KrakenDepthParser(ObjectMapper mapper) {
      this.counter = new AtomicLong();
      this.mapper = mapper;
   }
   
   
   /*
     [
        33904,
        [
          6247536191,
          0,
          -1
        ]
      ]
    */
   public Depth parseDepth(CurrencyPair pair, String source) {
      try {
         Object xx = pair.getBase() == Currency.BTC ? "xbt" : pair.getBase();
         String root = ("X" + xx + "Z" + pair.getQuote()).toUpperCase();
         String root2 = (""+xx + pair.getQuote()).toUpperCase();
         Map message = mapper.readValue(source, Map.class);
         Map result = (Map)message.get("result");
         
         if(result != null) {
            Map x = (Map)result.get(root);
            if(x==null){
               x=(Map)result.get(root2);
            }
            List asks = (List)x.get("asks");
            List bids = (List)x.get("bids");
            
            if(log.isTraceEnabled()) {
               log.trace("Received message: " + source);
            }
            return parseOrderBook(pair, asks, bids);
         }
      } catch (Exception e) {
         throw new IllegalStateException("Could not parse depth message: " + source, e);
      }
      return null;
   }
   
   private Depth parseOrderBook(CurrencyPair pair, List<?> asks, List<?> bids) {
      PriceSeries bidPrices = parsePriceList(pair, Side.BID, bids);
      PriceSeries askPrices = parsePriceList(pair, Side.ASK, asks);
      long version = counter.getAndIncrement();
      long time = System.currentTimeMillis();
      
      return new Depth(pair, bidPrices, askPrices, DepthType.SNAPSHOT, Market.KRAKEN, version, time);
   }
   
   private PriceSeries parsePriceList(CurrencyPair pair, Side side, List<?> prices) {
     List<Price> matches = parseActiveOrders(pair, side, prices);
     return new PriceSeries(matches);
   }
   
   private List<Price> parseActiveOrders(CurrencyPair pair, Side side, List<?> orders) {
      return orders.stream()
            .filter(Objects::nonNull)
            .filter(entry -> entry instanceof List)
            .map(entry -> new OrderBookResponse(pair, (List)entry))
            .map(entry -> parseActiveOrder(entry, side))
            .collect(Collectors.toList());
   }
   
   private Price parseActiveOrder(OrderBookResponse response, Side side) {
      CurrencyPair pair = response.getPair();
      String order = response.getOrderId();
      double price = response.getPrice();
      double amount = response.getAmount();
      
      return new Price(pair, Market.KRAKEN, side, order, Math.abs(price), Math.abs(amount));
   }

   private Side parseSide(OrderBookResponse response) {
      double quantity = response.getAmount();
      return quantity >= 0 ? Side.BID : Side.ASK; 
   }
   
   private static List getListField(List list, int index) {
      Object value = list.get(index);
      
      if(List.class.isInstance(value)) {
         return (List)value;
      }
      return null;
   }
   
   private static Long getLongField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Long.parseLong(value);
      }
      return null;
   }

   private static Double getDoubleField(List list, int index) {
      String value = getField(list, index);

      if (value != null) {
         return Double.parseDouble(value);
      }
      return null;
   }

   private static String getField(List list, int index) {
      Object value = list.get(index);

      if (value != null) {
         return String.valueOf(value);
      }
      return null;
   }
   
   @AllArgsConstructor
   private class OrderBookResponse {

      private final CurrencyPair pair;
      private final List<?> list;
      
      public CurrencyPair getPair(){
         return pair;
      }

      public String getOrderId() {
         return getField(list, 2);
      }
      
      public Double getAmount() {
         return getDoubleField(list, 1);
      }

      public Double getPrice() {
         return getDoubleField(list, 0);
      }
   }

}
