package com.zuooh.market.ecb;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses=EcbProperties.class)
public class EcbConfiguration {

}
