package com.zuooh.market.ecb;

import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zuooh.market.Currency;
import com.zuooh.market.fx.ExchangeRateSource;

@Slf4j
@Component
public class EcbExchangeRateSource implements ExchangeRateSource{
   
   private static final String ECB_URL = "http://api.fixer.io/latest?base=";
   
   private final Map<Currency, Map<Currency, Double>> rates;
   private final ObjectMapper mapper;
   
   public EcbExchangeRateSource() {
      this.rates = new ConcurrentHashMap<Currency, Map<Currency, Double>>();
      this.mapper = new ObjectMapper();
   }
   
   @Override
   public Map<Currency, Double> getRates(Currency base) {
      Map<Currency, Double> match = rates.get(base);
      
      if(match == null) {
         match = getCurrentRates(base);
      }
      if(match != null) {
         return match;
      }
      return Collections.emptyMap();
   }
   
   private Map<Currency, Double> getCurrentRates(Currency base) {
      try {
         URL target = new URL(ECB_URL + base);
         InputStream stream = target.openStream();
         String body = IOUtils.toString(stream, "UTF-8");
         EcbRates result = mapper.readValue(body, EcbRates.class);
         
         if(result != null) {
            Map<Currency, Double> values = result.getRates();
            rates.put(base, values);
            return values;
         }
      } catch(Exception e){
         log.info("Could not parse rates", e);
      }
      return null;
   }
   
   @Data
   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class EcbRates {
      
      private Map<String, Double> rates;
      private String base;
      private String date;
      
      public Map<Currency, Double> getRates() {
         Map<Currency, Double> result = new TreeMap<Currency, Double>();
         
         if(!rates.isEmpty()) {
            Set<String> currencies = rates.keySet();
            
            for(String currency : currencies) {
               Double value = rates.get(currency);
               
               try {
                  result.put(Currency.valueOf(currency), value);
               }catch(Throwable e) {
                  log.info("Could not resolve currency " + currency);
               }
            }
         }
         return result;
      }
   }
}
